/**
 * Created by Andrey Kashcheev on 3/22/15.
 */
var Control = (function() {


    // Constructor
    function Control() {
        init();

    }

    function init(){

        $(":button").click(function(event) {
            var id = event.target.id;
            var route = '/censusService/' + id;
            console.log(route);//debug



            queryCensus(route, function(response){
                console.log(response);
            });
        });

    }


    function getStates(){
        console.log("getStates called");
    }

    function queryCensus(route, callback) {

        $.ajax({
            type: 'GET',
            url: route,
            dataType: 'json',
            data: {

            },
            success: callback,
            error: showError
        });
    }


    function showError(error){
        console.log(error);
    }


    return Control;

}());

$(document).ready(function() {
    'use strict';
    var control = new Control();
});
