/**
 * Created by Andrey Kashcheev on 3/22/15.
 */

var express = require('express');
var router = express.Router();
var request = require('request');
var url = require('./urlFactory.js');




router.get('/:id', function(req, res) {

    var id = req.params.id;
    selector[id](req, res);

});


function getStates(req, res){

    var parameters= {
        "categories" : ["total", "NAME"],
        "forOpr01" : "state",
        "forOpr02" : null,
        "inOpr01" : null,
        "inOpr02" : null
    };

    var options = {
        url: url.urlFactory.getServiceUrl(parameters),
        headers: {
            'Method' : 'GET',
            'User-Agent': 'request'
        }
    };

    queryCensus(options, res);

}



function getOneStateData(req, res){
    console.log("***getOneStateData called***");//debug

    var parameters= {
        "categories" : [req.query.race || "total"],
        "forOpr01" : "state",
        "forOpr02" : req.query.state,
        "inOpr01" : null,
        "inOpr02" : null
    };

    var options = {
        url: url.urlFactory.getServiceUrl(parameters),
        headers: {
            'Method' : 'GET',
            'User-Agent': 'request'
        }
    };


    queryCensus(options, res);
}

function getStateRaceData(req, res){
    console.log("***getStateRaceData called***");//debug

    var parameters= {
        "categories" : [req.query.race],
        "forOpr01" : "state",
        "forOpr02" : req.query.state,
        "inOpr01" : null,
        "inOpr02" : null
    };


    var options = {
        url: url.urlFactory.getServiceUrl(parameters),
        headers: {
            'Method' : 'GET',
            'User-Agent': 'request'
        }
    };

    queryCensus(options, res);
}

function getZips(req, res){

    var parameters= {
        "categories" : ["total"],
        "forOpr01" : "zip",
        "forOpr02" : "*",
        "inOpr01" : "state",
        "inOpr02" : "53"
    };

    var options = {
        url: url.urlFactory.getServiceUrl(parameters),
        headers: {
            'Method' : 'GET',
            'User-Agent': 'request'
        }
    };

    queryCensus(options, res);

}


function getOneZip(req, res){
    console.log("***getOneZip called***");//debug

    var parameters= {
        "categories" : ["total"],
        "forOpr01" : "zip",
        "forOpr02" : req.query.zip,
        "inOpr01" : "state",
        "inOpr02" : "53"
    };


    var options = {
        url: url.urlFactory.getServiceUrl(parameters),
        headers: {
            'Method' : 'GET',
            'User-Agent': 'request'
        }
    };

    queryCensus(options, res);

}


function queryCensus(options, res){

    request(options, function(error, response, body){
        var data = JSON.parse(body);
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.send(data);
    });

}

var selector = {
    getStates : getStates,
    getZips : getZips,
    getOneStateData : getOneStateData,
    getOneZip : getOneZip
};

module.exports = router;