'use strict';

/**
 * @ngdoc function
 * @name week06AngularTestInClassApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the week06AngularTestInClassApp
 */
angular.module('week06AngularTestInClassApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
