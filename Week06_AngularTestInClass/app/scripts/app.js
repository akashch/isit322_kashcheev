'use strict';

/**
 * @ngdoc overview
 * @name week06AngularTestInClassApp
 * @description
 * # week06AngularTestInClassApp
 *
 * Main module of the application.
 */
angular
  .module('week06AngularTestInClassApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/inClass', {
        templateUrl: 'views/inClass.html',
        controller: 'inClass'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
