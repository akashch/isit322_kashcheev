/**
 * Created by Andrey Kashcheev on 3/16/15.
 */
var express = require('express');
var router = express.Router();



/* GET users listing. */
router.get('/', function(req, res) {
    res.render('censusApi', { title: 'Census API page' });

});

module.exports = router;
