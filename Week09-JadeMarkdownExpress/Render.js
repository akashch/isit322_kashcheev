/**
 * Created by Andrey Kashcheev on 3/16/15.
 */
var jade = require('jade');
var fs = require('fs');
var md = require( "markdown" ).markdown;

options = {
    "filename": "Render.js",
    "title": "My Title",
    "basedir": "/home/bcuser"
};

//var files = [
//    {
//        "in": "views/ElvenSass.jade",
//        "out": "/development/web/CssGuide/ElvenSass.html"
//    }, {
//        "in": "views/web/index.jade",
//        "out": "/development/web/index.html"
//    }
//];

var theRender = (function(){

    var thePath;

    var ignoredFiles = [
        "mixins.jade",
        "layout.jade"
    ];

    function theRender(filePath){
        console.log("theRender constructor called");
        thePath = filePath;
        startRendering();

    }

    function startRendering(){

        if(thePath){
            readFilesNames(thePath, function(filesNames){
                filesNames.forEach(function(fileToProcess){
                    if(ignoredFiles.indexOf(fileToProcess) === -1){
                        if(fileToProcess.slice(-4) === "jade"){
                            convertJadeToHtml(thePath, fileToProcess);

                        }
                    }else{
                    }
                });
            });

        }

    }

    function writeFile(fileToProcess, html){

        fs.writeFile(process.env.GIT_HUB_IO + '/' + fileToProcess.replace("jade", "html"), html, function (err) {
            if (err) {
                console.log(err);
            } else {
                console.log('wrote file');
            }
        });

    }


    function convertJadeToHtml(path, fileToProcess){

        var html = jade.renderFile(path + "/" +fileToProcess, options);

        writeFile(fileToProcess, html);

    }


    function readFilesNames(filePath, callback){

        fs.readdir(filePath, function(err, files){

            callback(files);

        });

    }



    return theRender;

})();

new theRender(process.argv[2]);
