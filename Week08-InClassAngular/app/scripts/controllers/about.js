'use strict';

/**
 * @ngdoc function
 * @name week08InClassCordovaPluginApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the week08InClassCordovaPluginApp
 */
angular.module('week08InClassCordovaPluginApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
