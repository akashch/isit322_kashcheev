'use strict';

/**
 * @ngdoc function
 * @name week08InClassCordovaPluginApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the week08InClassCordovaPluginApp
 */
angular.module('week08InClassCordovaPluginApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
