'use strict';

/**
 * @ngdoc overview
 * @name week08InClassCordovaPluginApp
 * @description
 * # week08InClassCordovaPluginApp
 *
 * Main module of the application.
 */
angular
  .module('week08InClassCordovaPluginApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
