var express = require('express');
var router = express.Router();

var token = "4032b75baba1a21e299c6460e95ea4a11c248ed3";

var censusService = {
    "url": "http://api.census.gov/data/2010/sf1",
    "params": [
        "key",
        "get",
        "for"
    ]
};

var parameters  = {
    "key": token,
    "categories" : {
        "total": "P0010001",
        "whites": "P0080003",
        "blacks": "P0080004",
        "asians": "P0080006",
        "houseUnits": "H00010001",
        "occupiedHouseUnits": "H0100001",
        "NAME": "NAME"
    },
    "geo": {
        "state" : "state",
        "country" : "country"
    }
};

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Week04-UnitTestCensus' });
});

router.get('/getServiceData', function(request, response) {
    "use strict";

    console.log("getServiceData called");

    response.status(200).send({
        "message" : {"serviceData" : censusService, "serviceParameters" : parameters}
    });
});


module.exports = router;
