/**
 * Created by Andrey on 1/27/15.
 */

// Did we use that main?

require.config({
    baseUrl: './',
    paths: {
        states: '../public/javascripts/states',
        stateController: '../public/javascripts/stateController',
        url : '../public/javascripts/url',
        angular : "../public/javascripts/lib/angular"
    },
    shim: {
        'angular': {
            exports: 'angular'
        },
        'angular-route': {
            deps: ["angular"],
            exports: 'angular-route'
        },
        'angular-mocks': {
            deps: ["angular"],
            exports: 'angular-mocks'
        }
    }
});

require([require], function() {
    'use strict';

    return {};
});