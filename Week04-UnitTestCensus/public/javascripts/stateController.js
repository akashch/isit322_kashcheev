/**
 * Created by Andrey on 1/29/2015.
 */
define(['states'], function(states) {
    'use strict';

    console.log("In Controller Module");

    states.controller("stateController", function($scope, url) {

        console.log("inside stateController");

       // $scope.message = "This is message from stateController";

        url.getStates().then(function(message) {
            $scope.states = message;
        });

        $scope.search = function(){

            console.log("searched called");
            url.getStatePopulation($scope.userSelection).then(function(message) {
                console.log(message);
                $scope.pop = "Population : " + message[0];
            });
        };

    });


});