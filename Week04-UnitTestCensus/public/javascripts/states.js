/**
 * Created by Andrey on 1/29/2015.
 */
define(['angular', 'angular-route'], function(angular, angularroute) {
    'use strict';

    var states = angular.module('app', ['ngRoute']);

    states.config(function($routeProvider) {

        $routeProvider.when("/", {
            controller: "stateController",
            templateUrl: "partials/view01.html"
        });

        //$routeProvider.when("/view02", {
        //    controller: "ctrl02",
        //    templateUrl: "partials/view01.html"
        //});

    });


    return states;
});