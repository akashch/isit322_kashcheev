/**
 * Created by Andrey on 1/29/2015.
 */

require.config({
    baseUrl: '.',
    paths: {
        "jquery": 'javascripts/lib/jquery',
        'angular': 'javascripts/lib/angular',
        'angular-route': 'javascripts/lib/angular-route',
        "states": "javascripts/states",
        "stateController": "javascripts/stateController",
        "url": "javascripts/url"
    },
    shim: {
        'angular': {
            exports: 'angular'
        },
        'angular-route': {
            deps: ["angular"],
            exports: 'angular-route'
        }
    }
});

require(['angular'], function() {
    'use strict';
    require(['angular-route', 'jquery', 'states', 'stateController', 'url'], function(angularRoute, jquery, states, controller, url) {


        $(document).ready(function() {

            angular.bootstrap($("#myContainer"), ["app"]);

        });

    });

});