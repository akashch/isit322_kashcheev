/**
 * Created by Andrey on 1/29/2015.
 */
define(['states'], function(states) {
    'use strict';



    states.factory("url", function($q, $http) {

        var exports = {};

        var serviceData = {};
        var parameters = {};



        exports.getStates = function() {

            var defers = $q.defer();

            apiCall('/getServiceData', function(response){
                apiCall(buildUrl(response.message, "NAME"), function(data){
                    data.shift();
                    defers.resolve(data);
                });
            });

            return defers.promise;
        };


        exports.getStatePopulation = function(state){
            var defers = $q.defer();
            console.log(state);
            apiCall('/getServiceData', function(response){
                apiCall(buildUrl(response.message, "total", state), function(data){
                    defers.resolve(data.pop());
                });
            });

            return defers.promise;
        };



        function buildUrl(data, catName, stateName){

            var url = data.serviceData.url + "?";

            for(var i = 0; i < data.serviceData.params.length; i++){
                url += data.serviceData.params[i] + "=";
                switch(i){
                    case 0:
                        url += data.serviceParameters.key + "&";

                        break;
                    case 1:
                        url += data.serviceParameters.categories[catName] + "&";
                        break;
                    case 2:
                        stateName ? url += data.serviceParameters.geo.state + ":" + stateName : url += data.serviceParameters.geo.state;
                        break;

                }
            }

            return url;
        }


        function apiCall(queryUrl, callback){
            console.log(queryUrl);

            $http.get(queryUrl).success(function(response){
                callback(response);
            }).error(function(err){
                console.log("ERROR");
                console.log(err);
            });

        }


        return exports;

    });

});