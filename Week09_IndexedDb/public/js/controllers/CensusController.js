/**
 * Created by Andrey Kashcheev on 3/9/15.
 */
Zenit.CensusController = (function() {


    function CensusController(){
        console.log("CensusController called");
        init();
    }


    function init(){
        console.log("CensusController - init called");

    }


    CensusController.prototype.getStates = function getStates(callback){

        var urlParameters= {
            "categories" : ["total", "NAME"],
            "forOpr01" : "state",
            "forOpr02" : null,
            "inOpr01" : null,
            "inOpr02" : null
        };


        queryCensus(urlParameters, callback);

    };


    CensusController.prototype.getZips = function getStates(callback){

            var urlParameters= {
                "categories" : ["total"],
                "forOpr01" : "zip",
                "forOpr02" : "*",
                "inOpr01" : "state",
                "inOpr02" : "53"
            };


        queryCensus(urlParameters, callback);

    };

    function queryCensus(urlParameters, callback) {

        $.ajax({
            type: 'GET',
            url: '/censusService/queryCensus',
            dataType: 'json',
            data: {
                "urlParameters": urlParameters
            },
            success: callback,
            error: showError
        });
    }


    function showError(error){
        console.log(error);
    }

    return CensusController;
})();