/**
 * Created by Andrey Kashcheev on 3/8/15.
 */


Zenit.IndexDbController = (function() {


    var db;
    var request;
    var dbName;
    var dataStore;

    var modes = [
        "readonly",
        "readwrite",
        "versionchange"
    ];

    function IndexDbController(dbNameInit, callbackReport){
        console.log("IndexDbController constructor called");//debug

        dbName = dbNameInit;
        report = callbackReport || report;

    }



    IndexDbController.prototype.openDb = function(versionNumber, dataStoreInit, successFunc){
        console.log("IndexDbController - openDb called");//debug

        try {
            dataStore = dataStoreInit || {};
            request = window.indexedDB.open(dbName, versionNumber);
            request.onerror = onerror;
            request.onupgradeneeded = onupgradeneeded;
            request.onsuccess = successFunc || onOpenDbSuccess;

            return request;
        } catch(e) {
            throw e.message;
        }

    };

    IndexDbController.prototype.createNewDataStore = function(dbName, dataStoreInit, successFunc){
        console.log("IndexDbController - createNewDataStore called");//debug

        var request = window.indexedDB.open(dbName);
        request.onsuccess = function (e){
            var database = e.target.result;
            var version =  parseInt(database.version);
            database.close();
            dataStore = dataStoreInit || {};
            var secondRequest = window.indexedDB.open(dbName, version + 1);
            secondRequest.onupgradeneeded = onupgradeneeded;
            secondRequest.onsuccess = successFunc || onOpenDbSuccess;

            return secondRequest;
        }

    };

    IndexDbController.prototype.getDbVersion = function(dbName, callback){
        console.log("IndexDbController - getDbVersion called");//debug

        var request = window.indexedDB.open(dbName);
        request.onsuccess = function (e){
            var database = e.target.result;
            database.close();
            callback(parseInt(database.version));
        }

    };

    IndexDbController.prototype.getDataStoresNames = function(dbName, callback){
        console.log("IndexDbController - getDbVersion called");//debug

        var request = window.indexedDB.open(dbName);
        request.onsuccess = function (e){
            var database = e.target.result;
            database.close();
            callback(database.objectStoreNames);
        }

    };


    IndexDbController.prototype.insert = function(versionNumber, dataStore, dataArray, callback) {
        console.log("IndexDbController - insertArray called");//debug

        this.openDb(versionNumber, dataStore, function(event) {
            db = event.target.result;
            var transaction = db.transaction([dataStore.name], modes[1]);

            // Do something when all the data is added to the database.
            transaction.oncomplete = function(event) {
                db.close();
                // $("#alerts").bootstrap_alert('alert-success', "Success", "Added Data.");
                if (typeof callback !== 'undefined') {
                    callback()
                }
            };

            transaction.onerror = onerror;

            var objectStore = transaction.objectStore(dataStore.name);
            for (var i in dataArray) {
                var request = objectStore.add(dataArray[i]);
                request.onsuccess = function(event) {
                    console.log(event);
                    // event.target.result == customerData[i].ssn;
                };
            }

        });

    };

    IndexDbController.prototype.queryDataStoreInRange = function(versionNumber, dataStoreName, callback){
        console.log("IndexDbController - queryDataStoreInRange called");//debug

        var that = this;

        that.openDb(versionNumber, null, function(event) {
            db = event.target.result;
            var transaction = db.transaction([dataStoreName]);
            var dataStore = transaction.objectStore(dataStoreName);

            var items = [];

            var cursorRequest = dataStore.openCursor(IDBKeyRange.only("98029"));

            transaction.oncomplete = function(evt) {
                db.close();
                callback(items);
            };

            cursorRequest.onerror = function(event) {
                report(false, "Database Failure", event.target.errorCode);
            };

            cursorRequest.onsuccess = function(event) {

                var cursor = event.target.result;
                if (cursor) {
                    items.push(cursor.value);
                    cursor.continue(); // I think this causes onsuccess to be called again
                }
            };

        });

    };


    IndexDbController.prototype.queryDataStore = function(versionNumber, dataStoreName, callback) {
        console.log("IndexDbController - openAndGetData called");//debug


        var that = this;

        that.openDb(versionNumber, null, function(event) {

            db = event.target.result;
            var transaction = db.transaction([dataStoreName]);
            var customerStore = transaction.objectStore(dataStoreName);


            var items = [];
            var cursorRequest = customerStore.openCursor();

            transaction.oncomplete = function(evt) {
                db.close();
                callback(items);
            };

            cursorRequest.onerror = function(event) {
                report(false, "Database Failure", event.target.errorCode);
            };

            cursorRequest.onsuccess = function(event) {

                var cursor = event.target.result;
                if (cursor) {
                    items.push(cursor.value);
                    cursor.continue(); // I think this causes onsuccess to be called again
                }
            };

        });
    };



    IndexDbController.prototype.clearDataStore = function(versionNumber, dataStoreName, callback) {

        var that = this;

        that.openDb(versionNumber, null, function(event) {

            db = event.target.result;

            var transaction = db.transaction([dataStoreName], modes[1]);
            var customerStore = transaction.objectStore(dataStoreName);

            transaction.oncomplete = function(event) {
                db.close();
                callback();
            };

            customerStore.clear().onsuccess = function(event) {
                console.log("object store cleared but operation not finished");
            };

        });

    };

    IndexDbController.prototype.getDbName = function(versionNumber, callback) {
        console.log("IndexDbController - getDbName called");//debug

        this.openDb(versionNumber, null, function(event) {
            callback(event.target.result.name);
        });
    };

    IndexDbController.prototype.close = function() {
        if (db) {
            db.close();
        }
    };

    IndexDbController.prototype.deleteDatabase = function(callback) {
        if (db) {
            db.close();
        }
        var deleteDbRequest = window.indexedDB.deleteDatabase(dbName);
        deleteDbRequest.onsuccess = function (event) {
            callback(event);
        };
        deleteDbRequest.onerror = function (e) {
            console.log("Database error: " + e.target.errorCode);
        };
    };


    function onupgradeneeded(event) {
        console.log("IndexDbController - onupgradeneeded called");//debug
        console.log(event);//debug


        var db = event.target.result;

        console.log(db);
        console.log(dataStore);
        if(dataStore){

            //Creating objectStore
            var objectStore = db.createObjectStore(dataStore.name, { keyPath: dataStore.keyPath });

            //Creating indexes
            if(dataStore.indexes && dataStore.indexes.length > 0){
                for(var i = 0; i < dataStore.indexes.length; i++){
                    objectStore.createIndex(dataStore.indexes[i].name, dataStore.indexes[i].field, { unique: dataStore.indexes[i].unique });
                }
            }


            objectStore.transaction.oncomplete = function(event) {
                var customerTransaction = db.transaction(dataStore.name, modes[dataStore.mode]);
                var customerObjectStore = customerTransaction.objectStore(dataStore.name);

                customerTransaction.oncomplete = function(event) {
                    db.close();
                    console.log("On upgrade complete");
                };

                for (var i in dataStore.data) {
                    customerObjectStore.add(dataStore.data[i]);
                }
            }

        }
    }


    function onOpenDbSuccess(event, callback) {
        db = event.target.result;
        console.log("OnGetDbSuccess called");
        report(true, "OnGetDbSuccess",  "Report: Opened Database");
    }


    function onerror(event) {
        console.log(JSON.stringify(event, null, 4));
        report(false, "Database Failure", event.target.errorCode);
    }


    function report(success, title, message) {
        console.log(title, message);
    }



    return IndexDbController;

})();