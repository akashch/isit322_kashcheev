/**
 * Created by Andrey Kashcheev on 3/8/15.
 */
var Zenit = {};

Zenit.AppControl = (function(){


    var _instance = null;

    var myDb;
    var censusController;

    var stateDataStore = {
        name : "state-census",
        keyPath : "stateNumber",
        data : [

        ],
        indexes : [
            {
                name : "name",
                field : "name",
                unique : true
            }, {
                name : "population",
                field : "population",
                unique : false
            }
        ],
        mode : 1
    };


    var zipDataStore = {
        name : "zip-census",
        keyPath : "zipNumber",
        data : [

        ],
        indexes : [
            {
                name : "zipNumber",
                field : "zipNumber",
                unique : true
            }, {
                name : "population",
                field : "population",
                unique : false
            }
        ],
        mode : 1
    };

    var dbName = "census";

    function AppControl(){
        console.log("AppControl called");//debug
        if (_instance === null) {
            _instance = this;

            init();
        }else{
            return _instance;
        }

    }

    function init(){
        console.log("init called");//debug
        myDb = new Zenit.IndexDbController(dbName);
        censusController = new Zenit.CensusController();


        addEventListeners();
        getDataStoreNames();
    }

    function getDataStoreNames(){

        myDb.getDataStoresNames(dbName, function(dsNames){
            $.each(dsNames, function(key, value) {
                $('#dropDown').append($("<option></option>").attr("value",key).text(value));
            });
        });
    }

    function addEventListeners(){

        //$("#openDb").click(openDb);
        //$("#getData").click(getData);
        //$("#getDbName").click(getDbName);
        //$("#deleteDB").click(deleteDB);
        //$("#clearStore").click(clearStore);
        //$("#insert").click(insert);

        $("#pageOne").click(function(){
            $("#viewsArea").load("/about/pageOne");
        });

        $("#pageTwo").click(function(){
            $("#viewsArea").load("/about/pageTwo");

        });

        $("#getStates").click(function(){

            censusController.getStates(function(response){
                processAndFormatStatesData(response);
            });

        });

        $("#getZips").click(function(){

            censusController.getZips(function(response){
                processAndFormatZipsData(response);
            });

        });


        $("#queryDataStore").click(function(){
            getData($("#dropDown :selected").text());

        });

        $("#issaqPop").click(function(){
            getIssaquahPopulation("zip-census");

        });


    }

    function processAndFormatStatesData(data){

        data.shift();
        data.forEach(function(state){
            stateDataStore.data.push({stateNumber :state[2], population : state[0], name : state[1]})
        });

        myDb.createNewDataStore(dbName, stateDataStore);

    }


    function processAndFormatZipsData(data){

        data.shift();
        data.forEach(function(state){
            zipDataStore.data.push({zipNumber :state[2], population : state[0], stateNumber : state[1]})
        });


        myDb.createNewDataStore(dbName, zipDataStore);

    }

    //**************************************************************************

    //function insert(item){
    //    console.log("insert called");//debug
    //
    //    myDb.insert(dbVersion, dataStore, [item], function(response){
    //        console.log(response);
    //    });
    //
    //}
    //
    //function clearStore(dataStoreName){
    //    console.log("clearStore called");//debug
    //
    //    myDb.getDbVersion(dbName, function(version){
    //        myDb.clearDataStore(version, dataStoreName, function(response){
    //            console.log(response);
    //        });
    //    });
    //}
    //
    //
    //function deleteDB(){
    //    console.log("deleteDB called");//debug
    //
    //    myDb.deleteDatabase(function(response){
    //
    //        console.log(response);
    //    });
    //
    //}
    //
    //function openDb(){
    //    console.log("createDb called");//debug
    //    myDb.getDbVersion(dbName, function(version){
    //        myDb.openDb(version, stateDataStore);
    //    });
    //}
    //
    //
    //
    function getData(dataStoreName){
        console.log("createDb called");//debug

        myDb.getDbVersion(dbName, function(version){
            myDb.queryDataStore(version, dataStoreName, function(e){
                console.log(e);
            });
        });

    }

    function getIssaquahPopulation(dataStoreName){
        console.log("createDb called");//debug

        myDb.getDbVersion(dbName, function(version){
            myDb.queryDataStoreInRange(version, dataStoreName, function(e){
                console.log(e);
            });
        });

    }
    //
    //function getDbName(){
    //    console.log("getDbName called");//debug
    //
    //    myDb.getDbVersion(dbName, function(version){
    //        var dbName = myDb.getDbName(version, function(dbName){
    //            console.log("---> " + dbName);
    //        });
    //    });
    //}
    //**************************************************************************

    return AppControl;

})();


$(document).ready(function(){

    new Zenit.AppControl();


});