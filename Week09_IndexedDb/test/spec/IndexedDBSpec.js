/**
 * Created by Andrey Kashcheev on 3/9/15.
 */
(function () {
    'use strict';

    describe('Week09-IndexedDb UnitTests', function () {

        var dataStore = {
            name : "customers",
            keyPath : "ssn",
            data : [
                {ssn: "444-44-4444", name: "Bill", age: 35, email: "bill@company.com"},
                {ssn: "555-55-5555", name: "Donna", age: 32, email: "donna@home.org"},
                {ssn: "666-66-6666", name: "Lisa", age: 52, email: "lisa@home.org"},
                {ssn: "777-66-6666", name: "Andrey", age: 54, email: "lisaM@home.org"}
            ],
            indexes : [
                {
                    name : "name",
                    field : "name",
                    unique : false
                }, {
                    name : "email",
                    field : "email",
                    unique : true
                }
            ],
            mode : 1
        };


        var dbName = "testDb";

        it('shows that we can run tests', function () {


            expect(1).to.equal(1);
        });


        it('shows that we can instantiate IndexDbController', function () {
            var index = new Zenit.IndexDbController(dbName);

            expect(index).to.be.ok;
        });

        it('shows we can open a db', function (done) {
            var index = new Zenit.IndexDbController(dbName);
            console.log(index);
            var request = index.openDb(1, dataStore, function(event){
                var db = event.target.result;
                expect(db).to.be.ok;
                expect(db.name).to.equal(dataStoredbName);
                db.close();
                done();
            });
        });


    });
})();
