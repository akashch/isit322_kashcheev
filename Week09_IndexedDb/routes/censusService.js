/**
 * Created by Andrey Kashcheev on 3/9/15.
 */
var express = require('express');
var router = express.Router();
var request = require('request');
var url = require('./urlFactory.js');



var options = {
    url: '',
    headers: {
        'Method' : 'GET',
        'User-Agent': 'request'
    }
};


router.get('/queryCensus', function(req, res) {
    console.log(req.query.urlParameters);
    options.url = url.urlFactory.getServiceUrl(req.query.urlParameters);
    request(options, function(error, response, body){
        var info = JSON.parse(body);
        res.send(info);
    });
});



module.exports = router;