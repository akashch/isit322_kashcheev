/**
 * Created by Andrey Kashcheev on 3/9/15.
 */
var express = require('express');
var router = express.Router();

/* GET home page. */

router.get('/', function(req, res) {
    res.render('about', { title: 'Week09-IndexedDb - About Page' });
});


router.get('/pageOne', function(req, res) {
    res.render('pageOne', { title: 'Week09-IndexedDb - Page One' });
});

router.get('/pageTwo', function(req, res) {
    res.render('pageTwo', { title: 'Week09-IndexedDb - Page Two' });
});

module.exports = router;