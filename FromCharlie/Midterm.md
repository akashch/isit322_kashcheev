# Midterm

Your wrote:

```
describe('Controller: ZipsCtrl', function () {

  // load the controller's module
  beforeEach(module('midtermKashcheevApp'));

  var ZipsCtrl, scope, responsesData, urls, methodsName;
  var $http, $httpBackend;
  
```

I would put variable declarations at the top of the file:


```

describe('Controller: ZipsCtrl', function () {

  var ZipsCtrl, scope, responsesData, urls, methodsName;
  var $http, $httpBackend;

  // load the controller's module
  beforeEach(module('midtermKashcheevApp'));

  
```

On your bower.json, I would select !1 to add this bit at the bottom, so
you don't have to see a menu when running bower install:

```
  "resolutions": {
    "angular": "1.2.8"
  }
```

Like this:

```
{
  "name": "midterm-kashcheev",
  "version": "0.0.0",
  "dependencies": {
    "angular": "1.2.8",
    "bootstrap": "^3.2.0",
    "angular-animate": "1.2.8",
    "angular-cookies": "1.2.8",
    "angular-resource": "1.2.8",
    "angular-route": "1.2.8",
    "angular-sanitize": "1.2.8",
    "angular-touch": "1.2.8",
    "angular-bootstrap": "~0.12.0",
    "angular-google-chart": "~0.0.11"
  },
  "devDependencies": {
    "angular-mocks": "1.2.8",
    "chai": "~2.0.0"
  },
  "appPath": "app",
  "resolutions": {
    "angular": "1.2.8"
  }
}
```
