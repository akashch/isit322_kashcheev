/**
 * Created by charliecalvert on 1/13/15.
 */

exports.stringSlices = {

    getFirstThreeCharacters: function(value) {
        'use strict';
        return value.slice(0, 3);
    },

    getLastThreeCharacters: function(value) {
        'use strict';

        return value.slice(-3);
    },
    getMiddleThreeCharacters : function(value){
        'use strict';

        if(value.length > 0 && value.length % 2 === 0){
            return value.slice(Math.round(value.length/2)-1, (Math.round(value.length/2)-1) + 3);
        }else if(value.length > 0 && value.length % 2 !== 0){
            return value.slice(Math.round(value.length/2)-2, (Math.round(value.length/2)-2) + 3);
        }
    },
    getAllButFirstAndLast : function(value){
        'use strict';

        return value.slice(1,value.lenght).slice(0, -1);
    },
    getAllButFirstAndLastIfSame : function(value){
        'use strict';
        if(value.slice(0,1) === value.slice(-1)){
            return value.slice(1,value.lenght).slice(0, -1);
        }else{
            return value;
        }
    }
    
};
