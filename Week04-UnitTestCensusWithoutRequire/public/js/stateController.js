/**
 * Created by Andrey on 2/3/2015.
 */


states.controller("stateController", function($scope, url) {

    console.log("inside stateController");

    // $scope.message = "This is message from stateController";

    //url.getStates().then(function(message) {
    //    $scope.states = message;
    //});


    $scope.getParameters = function(){

        url.getParameters().then(function(message){
            $scope.parameters = message;
        });

    };

    $scope.search = function(){

        console.log("searched called");
        url.getStatePopulation($scope.userSelection).then(function(message) {
            console.log(message);
            $scope.pop = "Population : " + message[0];
        });
    };

});
