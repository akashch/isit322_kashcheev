/**
 * Created by Andrey on 2/3/2015.
 */


var states = angular.module('app', ['ngRoute']);


states.config(function($routeProvider) {
    "use strict";
    $routeProvider.when("/", {
        controller: "stateController",
        templateUrl: "partials/view01.html"
    });

    //$routeProvider.when("/view02", {
    //    controller: "ctrl02",
    //    templateUrl: "partials/view01.html"
    //});

});
