/**
 * Created by Andrey on 2/3/2015.
 */


describe("Week04-UnitTestsCensus", function() {
    'use strict';
    var myController = null;
    var $httpBackend = null;

    beforeEach(function() {
        module('app');
    });

    beforeEach(inject(function($rootScope, $controller) {
        myController = $rootScope.$new();
        $controller('stateController', { $scope: myController });
    }));

    beforeEach(inject(function(_$httpBackend_) {
        $httpBackend = _$httpBackend_;
    }));

    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it("Proves that we can run tests", function() {
        expect(true).toBe(true);
    });

    it("Test load json with settings", function() {
        $httpBackend.expectGET('/getServiceData').respond({
            "message" : {"serviceData" : {
                "url": "http://api.census.gov/data/2010/sf1",
                "params": [
                    "key",
                    "get",
                    "for"
                ]
            }, "serviceParameters" : {
                "key": "4032b75baba1a21e299c6460e95ea4a11c248ed3",
                "categories" : {
                    "total": "P0010001",
                    "whites": "P0080003",
                    "blacks": "P0080004",
                    "asians": "P0080006",
                    "houseUnits": "H00010001",
                    "occupiedHouseUnits": "H0100001",
                    "NAME": "NAME"
                },
                "geo": {
                    "state" : "state",
                    "country" : "country"
                }
            }}
        });

        myController.getParameters();
        $httpBackend.flush();
        expect(myController.parameters.serviceData.url).toEqual("http://api.census.gov/data/2010/sf1");
    });

});
