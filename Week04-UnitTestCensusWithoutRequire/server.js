/**
 * Created by Andrey on 2/3/2015.
 */
var express = require('express');
var app = express();
var http = require('http').Server(app);
var path = require('path');
var request = require('request');

var token = "4032b75baba1a21e299c6460e95ea4a11c248ed3";

var censusService = {
    "url": "http://api.census.gov/data/2010/sf1",
    "params": [
        "key",
        "get",
        "for"
    ]
};

var parameters  = {
    "key": token,
    "categories" : {
        "total": "P0010001",
        "whites": "P0080003",
        "blacks": "P0080004",
        "asians": "P0080006",
        "houseUnits": "H00010001",
        "occupiedHouseUnits": "H0100001",
        "NAME": "NAME"
    },
    "geo": {
        "state" : "state",
        "country" : "country"
    }
};


app.use(express.static(path.join(__dirname, 'public')));


app.get('/', function(req, res) {
    "use strict";
    res.sendFile(__dirname + '/index.html');
});

app.get('/getServiceData', function(request, response) {
    "use strict";

    console.log("getServiceData called");

    response.status(200).send({
        "message" : {"serviceData" : censusService, "serviceParameters" : parameters}
    });
});


http.listen(30025, function() {
    "use strict";
    console.log('listening on http://localhost:30025');
});