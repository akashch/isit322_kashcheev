/**
 * Created by Andrey Kashcheev on 2/17/15.
 */
"use strict";


describe('Controller: StateGraphCtrl', function () {


  // load the controller's module
  beforeEach(module('week10AngularControllerAsUnitTestsApp'));

  var stateGraphCtrl, scope, responsesData, urls, methodsName;
  var $http, $httpBackend;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    stateGraphCtrl = $controller('StateGraphCtrl', {
      $scope: scope
    });

    urls = [
      "http://api.census.gov/data/2010/sf1?key=4032b75baba1a21e299c6460e95ea4a11c248ed3&get=NAME&for=state",
      "http://api.census.gov/data/2010/sf1?key=4032b75baba1a21e299c6460e95ea4a11c248ed3&get=P0010001&for=state:01",
      "http://api.census.gov/data/2010/sf1?key=4032b75baba1a21e299c6460e95ea4a11c248ed3&get=P0080003&for=state:01",
      "http://api.census.gov/data/2010/sf1?key=4032b75baba1a21e299c6460e95ea4a11c248ed3&get=NAME,P0010001&for=state",
      "http://api.census.gov/data/2010/sf1?key=4032b75baba1a21e299c6460e95ea4a11c248ed3&get=P0010001&for=state:*"
    ];

    methodsName = [
      "graphItems"
    ];

    responsesData = {

      allStates : [["NAME","P0010001","state"], ["Alabama","4779736","01"], ["Alaska","710231","02"], ["Arizona","6392017","04"], ["Arkansas","2915918","05"], ["California","37253956","06"], ["Colorado","5029196","08"], ["Connecticut","3574097","09"], ["Delaware","897934","10"], ["District of Columbia","601723","11"], ["Florida","18801310","12"], ["Georgia","9687653","13"], ["Hawaii","1360301","15"], ["Idaho","1567582","16"], ["Illinois","12830632","17"], ["Indiana","6483802","18"], ["Iowa","3046355","19"], ["Kansas","2853118","20"], ["Kentucky","4339367","21"], ["Louisiana","4533372","22"], ["Maine","1328361","23"], ["Maryland","5773552","24"], ["Massachusetts","6547629","25"], ["Michigan","9883640","26"], ["Minnesota","5303925","27"], ["Mississippi","2967297","28"], ["Missouri","5988927","29"], ["Montana","989415","30"], ["Nebraska","1826341","31"], ["Nevada","2700551","32"], ["New Hampshire","1316470","33"], ["New Jersey","8791894","34"], ["New Mexico","2059179","35"], ["New York","19378102","36"], ["North Carolina","9535483","37"], ["North Dakota","672591","38"], ["Ohio","11536504","39"], ["Oklahoma","3751351","40"], ["Oregon","3831074","41"], ["Pennsylvania","12702379","42"], ["Rhode Island","1052567","44"], ["South Carolina","4625364","45"], ["South Dakota","814180","46"], ["Tennessee","6346105","47"], ["Texas","25145561","48"], ["Utah","2763885","49"], ["Vermont","625741","50"], ["Virginia","8001024","51"], ["Washington","6724540","53"], ["West Virginia","1852994","54"], ["Wisconsin","5686986","55"], ["Wyoming","563626","56"], ["Puerto Rico","3725789","72"]]

    };

  }));


  beforeEach(inject(function(_$http_, _$httpBackend_) {
    $http = _$http_;
    $httpBackend = _$httpBackend_;
  }));

  afterEach(function() {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  function getData(inputParameters) {

    $httpBackend.whenGET(urls[inputParameters.urlIndex]).respond(responsesData[inputParameters.responseData]);

    stateGraphCtrl.userSelection = inputParameters.stateNumber;
    stateGraphCtrl.userSelection2 = inputParameters.category;

    stateGraphCtrl[methodsName[inputParameters.methodIndex]](inputParameters.sliceStartIndex, inputParameters.sliceEndIndex);

    $httpBackend.flush();
  }


  it('proves that we can run test for StateGraphCtrl', function () {
    expect(true).toBe(true);
  });

  it('proves that StateGraphCtrl has function called "grapItems" ', function () {
    expect(typeof stateGraphCtrl.graphItems).toBe("function");
  });


  it('should get data about all 52 states ', function() {

    var inputParameters = {
      urlIndex : 3,
      methodIndex : 0,
      stateNumber : null,
      category : null,
      responseData : "allStates"
    };

    getData(inputParameters);

    expect(stateGraphCtrl.items.length).toEqual(52);
  });

  it('should get data about top 10 states ', function() {

    var inputParameters = {
      urlIndex : 3,
      methodIndex : 0,
      stateNumber : null,
      category : null,
      responseData : "allStates",
      sliceStartIndex : 0,
      sliceEndIndex : 10
    };

    getData(inputParameters);

    expect(stateGraphCtrl.items.length).toEqual(10);
  });


  it('should get data about top 5 states ', function() {

    var inputParameters = {
      urlIndex : 3,
      methodIndex : 0,
      stateNumber : null,
      category : null,
      responseData : "allStates",
      sliceStartIndex : 0,
      sliceEndIndex : 5
    };

    getData(inputParameters);

    expect(stateGraphCtrl.items.length).toEqual(5);
  });


  it('should get data about most populated state ', function() {

    var inputParameters = {
      urlIndex : 3,
      methodIndex : 0,
      stateNumber : null,
      category : null,
      responseData : "allStates",
      sliceStartIndex : 0,
      sliceEndIndex : 1
    };

    getData(inputParameters);


    expect(stateGraphCtrl.items[0].c[0].v).toEqual("California");
  });

  it('should get data about less populated state ', function() {

    var inputParameters = {
      urlIndex : 3,
      methodIndex : 0,
      stateNumber : null,
      category : null,
      responseData : "allStates",
      sliceStartIndex : 0,
      sliceEndIndex : -1
    };

    getData(inputParameters);


    expect(stateGraphCtrl.items[0].c[0].v).toEqual("Wyoming");
  });


  it('should get 3 states in topStates with sliceStartIndex and sliceEndIndex ', function() {

    var inputParameters = {
      urlIndex : 3,
      methodIndex : 0,
      stateNumber : null,
      category : null,
      responseData : "allStates",
      sliceStartIndex : 2,
      sliceEndIndex : 5
    };

    getData(inputParameters);


    expect(stateGraphCtrl.items.length).toEqual(3);
    expect(stateGraphCtrl.items[0].c[0].v).toEqual("New York");
    expect(stateGraphCtrl.items[1].c[0].v).toEqual("Florida");
    expect(stateGraphCtrl.items[2].c[0].v).toEqual("Illinois");
  });

});
