/**
 * Created by Andrey Kashcheev on 2/17/15.
 */
'use strict';

/**
 * @ngdoc function
 * @name week06AngularChartCensusApp.controller:StatesCtrl
 * @description
 * # StatesCtrl
 * Controller of the midtermKashcheevApp
 */


angular.module('week10AngularControllerAsUnitTestsApp')
  .controller('StatesCtrl', function (censusService) {

    var statesCtrl = this;

    statesCtrl.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];


    var inputParameters= {
      "categories" : ["NAME"],
      "forOpr01" : "state",
      "forOpr02" : null,
      "inOpr01" : null,
      "inOpr02" : null
    };

    censusService.getStatesData(inputParameters).then(function(data) {
      statesCtrl.states = data;
      statesCtrl.races = censusService.races;
    });




    statesCtrl.search = function(){

      console.log(statesCtrl.userSelection);

      var inputParameters= {
        "categories" : ["total"],
        "forOpr01" : "state",
        "forOpr02" : statesCtrl.userSelection,
        "inOpr01" : null,
        "inOpr02" : null
      };

      censusService.getStatesData(inputParameters).then(function(data) {

        statesCtrl.pop = data[1][0];

      });

    };


    statesCtrl.searchForRace = function(){


      var inputParameters= {
        "categories" : [statesCtrl.userSelection2],
        "forOpr01" : "state",
        "forOpr02" : statesCtrl.userSelection,
        "inOpr01" : null,
        "inOpr02" : null
      };

      censusService.getStatesData(inputParameters).then(function(data) {
        statesCtrl.pop = data[1][0];
      });

    };

  });
