/**
 * Created by Andrey Kashcheev on 2/17/15.
 */

'use strict';

/**
 * @ngdoc function
 * @name week06AngularChartCensusApp.controller:ZipsCtrl
 * @description
 * # ZipsCtrl
 * Controller of the midtermKashcheevApp
 */


angular.module('week10AngularControllerAsUnitTestsApp')
  .controller('ZipsCtrl', function (censusService) {

    var zipsCtrl = this;

    zipsCtrl.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    var inputParameters= {
      "categories" : ["total"],
      "forOpr01" : "zip",
      "forOpr02" : "*",
      "inOpr01" : "state",
      "inOpr02" : "53"
    };

    censusService.getStatesData(inputParameters).then(function(data) {

      data.shift();
      zipsCtrl.states = data;

    });




    zipsCtrl.getZipPopulation = function(){

      var inputParameters= {
        "categories" : ["total"],
        "forOpr01" : "zip",
        "forOpr02" : zipsCtrl.userSelection[2],
        "inOpr01" : "state",
        "inOpr02" : "53"
      };

      censusService.getStatesData(inputParameters).then(function(data) {
        zipsCtrl.pop = data[1][0];
      });

    };


  });
