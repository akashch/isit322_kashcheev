'use strict';

/**
 * @ngdoc overview
 * @name week10AngularControllerAsUnitTestsApp
 * @description
 * # week10AngularControllerAsUnitTestsApp
 *
 * Main module of the application.
 */
angular.module("utils", []);

angular
  .module('week10AngularControllerAsUnitTestsApp', [
    "utils",
    'googlechart',
    'ui.bootstrap',
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: "views/main.html",
        controller: 'MainCtrl',
        controllerAs: 'mainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'aboutCtrl'
      })
      .when('/states', {
        templateUrl: 'views/states.html',
        controller: 'StatesCtrl',
        controllerAs: 'statesCtrl'
      })
      .when('/stateGraph', {
        templateUrl: 'views/stateGraph.html',
        controller: 'StateGraphCtrl',
        controllerAs: 'stateGraphCtrl'
      })
      .when('/zips', {
        templateUrl: 'views/zips.html',
        controller: 'ZipsCtrl',
        controllerAs: 'zipsCtrl'
      })
      .when('/zipGraph', {
        templateUrl: 'views/zipGraph.html',
        controller: 'ZipGraphCtrl',
        controllerAs: 'zipGraphCtrl'
      })
      .when('/nodeRoute', {
        templateUrl: 'views/nodeRoute.html',
        controller: 'NodeRouteCtrl',
        controllerAs: 'nodeRouteCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  }).controller('HeaderController', function($scope, $location)
  {
    $scope.isActive = function (viewLocation) {
      return viewLocation === $location.path();
    };
  });
