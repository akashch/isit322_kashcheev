/**
 * Created by Andrey on 1/18/15.
 */

var fs = require('fs');

/**
 * We'll work on this one later.
 */
exports.getHtml = function (json, fieldNames) {
    'use strict';

    var html = '<div>\n\t<ul>\n';
    var parsed = JSON.parse(json);

    if(parsed.length > 0){

        parsed.forEach(function(item){
            html +='\t\t<li>\n';
            for(var elem in item){
                html +='\t\t\t<p><strong>' + elem + "</strong> : " + item[elem] + '</p>\n';
            }
            html +='\t\t</li>\n';
        });
    }
    html += '\t</ul>\n</div>';

    return html;

};

