/**
 * Created by Andrey on 1/18/15.
 */

var fs = require('fs');


var writeHtml = function(fileName, html, callback){
    'use strict';
    console.log(fileName);
    console.log(html);

    fs.writeFile(fileName, html, function(err) {

        if(err){
            console.log(err);
            callback({status : 500, errMessage : err});
        }else{
            console.log(fileName + " saved.");
            callback({status : 201});
        }

    });
};


var writeJson = function(fileName, json, callback){
    'use strict';

    fs.writeFile(fileName, JSON.stringify(json, null, 4), function(err) {

        if(err){
            console.log(err);
            callback({status : 500, errMessage : err});
        }else{
            console.log(fileName + " saved.");
            callback({status : 201});
        }

    });

};


var readJson = function(fileName){
    'use strict';

    return fs.readFileSync(fileName, 'utf8');
};

exports.readJson = readJson;
exports.writeHtml = writeHtml;
exports.writeJson = writeJson;