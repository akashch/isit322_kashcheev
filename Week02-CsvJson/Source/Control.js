var csv = require("./ParseCsv");
var fileWriter = require("./FileWriters");
var createHtml= require("./CreateHtml");
var createTests = require("./CreateTests");
var fileIndex = 0;
var coreName = "IsitMeetings";
var fileNames = ["Data/" + coreName + ".csv", coreName + ".tsv", "Output/"+ coreName + ".json"];
var fileTypes = ["csv", "tsv"];


var converter = new csv.ConvertToJson();
converter.fileWriter = fileWriter;
converter.createHtml = createHtml;

var json = converter.run(fileNames[fileIndex], fileTypes[fileIndex]);

converter.fileWriter.writeJson(fileNames[2], json, function(result){
    'use strict';
    if(result.status === 201){

        converter.fileWriter.writeHtml("Output/" + coreName + ".html",converter.createHtml.getHtml(converter.fileWriter.readJson(fileNames[2]), converter.defineHeaderTokens()), function(result){

            if(result.status === 201){

                createTests.getTest(json);
                createTests.getKeyTests(json);

            }

        });

    }
});





//var json = csv.convertToJson(fileNames[fileIndex], fileTypes[fileIndex]);
//console.log(json);
/*
csv.writeJson("Output/" + coreName + ".json", json);
var html = createHtml.getHtml(json, ['day', 'meeting_name', 'city']);
csv.writeHtml("Output/" + coreName + ".html", html);
createTests.getTest(json);
createTests.getKeyTests(json);
*/