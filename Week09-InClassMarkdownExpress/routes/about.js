/**
 * Created by bcuser on 3/4/15.
 */
var express = require('express');
var router = express.Router();

/* GET home page. */

router.get('/', function(req, res) {
    res.render('about', { title: 'About In Class Markdown Express' });
});


router.get('/garply', function(req, res) {
    res.render('garply', { title: 'About garply In Class Markdown Express' });
});

module.exports = router;
