/**
 * Created by Andrey on 1/26/2015.
 */
define([require], function() {
    'use strict';

    var _instance = null;
    var apiKey = "4032b75baba1a21e299c6460e95ea4a11c248ed3"; //We probably should store it in Couch DB

    var categories = {"total" : 'P0010001', "whites" : 'P0080003', "blacks" : 'P0080004',"asians": 'P0080006' , "houseUnits" : 'H00010001' , "occupiedHouseUnits" : 'H0100001', "NAME" : "NAME"};
    var statesNames = "http://api.census.gov/data/2010/sf1?key=" + apiKey + "&get=" + categories.NAME +"&for=state:*";

    function ApiControl() {

        if (_instance === null) {
            _instance = this;

            /* test-code */

            Object.defineProperty(this, "apiKey", {
                get: function() {
                    return apiKey;
                },
                enumerable: false,
                configurable: true
            });


            Object.defineProperty(this, "categories", {
                get: function() {
                    return categories;
                },
                enumerable: true,
                configurable: true
            });


            Object.defineProperty(this, "init", {
                get: function() {
                    return init;
                },
                enumerable: false,
                configurable: true
            });

            Object.defineProperty(this, "displayData", {
                get: function() {
                    return displayData;
                },
                enumerable: false,
                configurable: true
            });


            Object.defineProperty(this, "apiCall", {
                get: function() {
                    return apiCall;
                },
                enumerable: false,
                configurable: true
            });
            /* end-test-code */

            init();
        } else {
            return _instance;
        }
    }

    function init(){
        apiCall(statesNames, function(data){
            data.map(function(item){
                if(item[0] !== "NAME"){
                    $("#menu").append("<option  data_index=" + item[1] + ">" + item[0] + "</option >");
                }

            });
        });

        $("#search").click(function(){
            var params = "&for=state:" + $("#menu option:selected").attr('data_index');
            var queryUrl = "http://api.census.gov/data/2010/sf1?key=" + apiKey + "&get=" + categories.total+ "," + categories.NAME + "" + params;

            apiCall(queryUrl, function(data){
                displayData(data);
            });

        });
    }

    function displayData(data){

        var i, j;

        $("#searchData").empty();
        $("#searchData").append("<p>Response: </p>");

        data.map(function(item) {


            item.map(function(item){

                $("#searchData").append("<li>" + item + "</li>");

            });

            if(item.indexOf(categories.total) > -1 && item.indexOf(categories.NAME) > -1){

                i = item.indexOf(categories.total);
                j = item.indexOf(categories.NAME);

            }else{

                $("#searchData").append("<br/>");
                $("#searchData").append("<li> State: " + item[j] + "</li>");
                $("#searchData").append("<li> Population: " + item[i] + "</li>");

            }
        });

    }



    function apiCall(queryUrl, callback){
        console.log(queryUrl);
        $.ajax({
            type: 'GET',
            url: queryUrl,
            success: function(data){
                callback(data);
            },
            error: function(err){
                console.log(err);
            }
        });

    }



    return ApiControl;

});