/**
 * Created by Andrey on 1/26/2015.
 */
define(["ApiControl"], function(ApiControl) {
    'use strict';

    var apiControl;


    function Control(){
        //console.log("Control called");//debug

        /* test-code */
        Object.defineProperty(this, "init", {
            get: function() {
                return init;
            },
            enumerable: false,
            configurable: true
        });
        /* end-test-code */

        init();

    }


    function init(){

        apiControl = new ApiControl();

    }


    return Control;
});