/**
 * Created by Andrey on 1/26/2015.
 */

require.config({
    baseUrl : '.',
    paths : {
        "jquery" : 'javascripts/lib/jquery',
        "Control" : "javascripts/controls/Control",
        "ApiControl" : "javascripts/controls/ApiControl"
    },
    shim : {
        "TinyPubSub": {
            deps: ["jquery"],
            exports: "TinyPubSub"
        }
    }
});

require([ 'jquery', 'Control'],
    function(jq, Control) {

        'use strict';

        $(document).ready(function() {
            new Control();
        });

    });