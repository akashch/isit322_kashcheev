'use strict';

/**
 * @ngdoc function
 * @name week05YeomanAngularCensusApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the week05YeomanAngularCensusApp
 */
angular.module('week05YeomanAngularCensusApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    console.log('bootstrap setup');

    $scope.singleModel = 0;

    $scope.radioModel = 'Middle';

    $scope.checkModel = {
      left: false,
      middle: true,
      right: false
    };

  });
