/**
 * Created by bcuser on 2/5/15.
 */
'use strict';

/**
 * @ngdoc function
 * @name week05YeomanAngularCensusApp.controller:StateController
 * @description
 * # StateController
 * Controller of the week05YeomanAngularCensusApp
 */
angular.module('week05YeomanAngularCensusApp')
  .controller('StatesController', function($scope, url){
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    console.log('bootstrap setup');


    url.getStates().then(function(data) {
      $scope.states = data;
      $scope.races = url.races;
    });



    $scope.search = function(){


      url.getPopulation($scope.userSelection).then(function(data) {
        $scope.pop = data[1][0];
      });

    };


    $scope.searchForRace = function(){

      url.searchForRace($scope.userSelection, $scope.userSelection2).then(function(data) {
        $scope.pop = data[1][0];
      });

    };

  });
