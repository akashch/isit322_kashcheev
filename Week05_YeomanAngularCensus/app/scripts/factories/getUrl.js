/**
 * Created by bcuser on 2/7/15.
 */
'use strict';

/**
 * @ngdoc function
 * @name week05YeomanAngularCensusApp.factory:getUrl
 * @description
 * # getUrl
 * Factory of the week05YeomanAngularCensusApp
 */


var mod = angular.module("week05YeomanAngularCensusApp");

mod.factory("url", function($q, $http) {


  var exports = {};
  console.log($q);

  var token = "4032b75baba1a21e299c6460e95ea4a11c248ed3";
  var categories = {"total" : 'P0010001', "whites" : 'P0080003', "blacks" : 'P0080004',"asians": 'P0080006' , "houseUnits" : 'H00010001' , "occupiedHouseUnits" : 'H0100001', "NAME" : "NAME"};
  var statesNames = "http://api.census.gov/data/2010/sf1?key=" + token + "&get=" + categories.NAME +"&for=state:*";

  var censusService = {
    "url": "http://api.census.gov/data/2010/sf1",
    "params": [
      "key",
      "get",
      "for"
    ]
  };

  var parameters  = {
    "key": token,
    "categories" : {
      "total": "P0010001",
      "whites": "P0080003",
      "blacks": "P0080004",
      "asians": "P0080006",
      "houseUnits": "H00010001",
      "occupiedHouseUnits": "H0100001",
      "NAME": "NAME"
    },
    "geo": {
      "state" : "state",
      "country" : "country"
    }
  };

  exports.races = ["total", "whites", "blacks", "asians"];

  var API_DATA = {"serviceData" : censusService, "serviceParameters" : parameters};




  exports.getStates = function() {

    var defers = $q.defer();


    apiCall(statesNames,function(data){
      defers.resolve(data);
    });

    return defers.promise;
  };


  exports.getPopulation = function(userSelection01){

    var defers = $q.defer();

    apiCall(buildUrl(API_DATA, "total", userSelection01), function(data){
      console.log(data);
      defers.resolve(data);
    });

    return defers.promise;
  };

  exports.searchForRace = function(userSelection01, userSelection02){

    var defers = $q.defer();

    apiCall(buildUrl(API_DATA, userSelection02, userSelection01), function(data){
      console.log(data);
      defers.resolve(data);
    });

    return defers.promise;
  };



  function buildUrl(data, catName, stateName){

    var url = data.serviceData.url + "?";

    for(var i = 0; i < data.serviceData.params.length; i++){
      url += data.serviceData.params[i] + "=";
      switch(i){
        case 0:
          url += data.serviceParameters.key + "&";

          break;
        case 1:
          url += data.serviceParameters.categories[catName] + "&";
          break;
        case 2:
          stateName ? url += data.serviceParameters.geo.state + ":" + stateName : url += data.serviceParameters.geo.state;
          break;

      }
    }

    return url;
  }



  function apiCall(queryUrl, callback){
    console.log(queryUrl);

    $http.get(queryUrl).success(function(response){
      callback(response);
    }).error(function(err){
      console.log("ERROR");
      console.log(err);
    });

  }



  return exports;

});
