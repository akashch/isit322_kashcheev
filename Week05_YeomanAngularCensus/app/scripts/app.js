'use strict';

/**
 * @ngdoc overview
 * @name week05YeomanAngularCensusApp
 * @description
 * # week05YeomanAngularCensusApp
 *
 * Main module of the application.
 */
angular
  .module('week05YeomanAngularCensusApp', [
    'googlechart',
    'ui.bootstrap',
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when("/statePop", {
        templateUrl: "views/statePop.html",
        controller: "StatesController"
      })
      .otherwise({
        redirectTo: '/'
      });
  });
