'use strict';

/**
 * @ngdoc function
 * @name week06AngularTestInClassApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the week06AngularTestInClassApp
 */
angular.module('week06AngularTestsAtHomeApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
