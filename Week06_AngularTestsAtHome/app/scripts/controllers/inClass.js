/**
 * Created by bcuser on 2/11/15.
 */
'use strict';

/**
 * @ngdoc function
 * @name week06AngularTestInClassApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the week06AngularTestInClassApp
 */
angular.module('week06AngularTestsAtHomeApp')
  .controller('inClass', function ($scope, titleFactory) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
      $scope.title = titleFactory.getTitle();


  });
