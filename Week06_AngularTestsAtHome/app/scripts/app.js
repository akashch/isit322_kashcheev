'use strict';

/**
 * @ngdoc overview
 * @name week06AngularTestsAtHomeApp
 * @description
 * # week06AngularTestsAtHomeApp
 *
 * Main module of the application.
 */
angular
  .module('week06AngularTestsAtHomeApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/inClass', {
        templateUrl: 'views/inClass.html',
        controller: 'inClass'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
