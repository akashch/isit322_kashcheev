/**
 * Created by bcuser on 2/11/15.
 */
'use strict';

describe('Controller: AboutCtrl', function () {

  // load the controller's module
  beforeEach(module('week06AngularTestsAtHomeApp'));

  var inClass,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    inClass = $controller('inClass', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });


  it('title field should be isit322 - HelloWorld', function () {
    var result = scope.title
    expect(result).toBe("isit322 - HelloWorld");
  });


});
