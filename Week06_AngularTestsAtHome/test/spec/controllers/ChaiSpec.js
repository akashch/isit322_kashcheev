/**
 * Created by bcuser on 2/11/15.
 */


describe('Controller: inClass', function () {
  'use strict';

  var expect = chai.expect;
  var inClass, scope, cities;
  var $http, $httpBackend;

  beforeEach(function() {
    module('week06AngularTestsAtHomeApp');
  });

  beforeEach(inject(function($controller, $rootScope) {
    scope = $rootScope.$new();
    inClass = $controller('inClass', {
      $scope: scope
    });

    cities = ["Seattle", "Vancouver", "St.Petersburg"];
  }));

  function getData() {
    $httpBackend.whenGET(scope.url).respond(allStates);
    scope.getCities();
    $httpBackend.flush();
  }

  it('proves that we can run tests', function() {
    expect(true).to.equal(true);
  });

  it('title field should be isit322 - HelloWorld', function () {
    var result = scope.title
    expect(result).to.equal("isit322 - HelloWorld");
  });


  it('title field should be isit322 - HelloWorld', function () {
    var result = scope.title
    expect(result).to.equal("isit322 - HelloWorld");
  });


  it('should get 3 cities', function() {
    getData();
    expect(scope.allCities.length).to.equal(3);
  });

});
