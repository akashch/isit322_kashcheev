'use strict';

/**
 * @ngdoc function
 * @name week10ControllerAsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the week10ControllerAsApp
 */
angular.module('week10ControllerAsApp')
  .controller('PerrieController', function () {
    var perrieController = this;

    perrieController.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    perrieController.description = "Pierre";

  });
