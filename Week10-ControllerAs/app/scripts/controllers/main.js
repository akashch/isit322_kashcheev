'use strict';

/**
 * @ngdoc function
 * @name week10ControllerAsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the week10ControllerAsApp
 */
angular.module('week10ControllerAsApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
