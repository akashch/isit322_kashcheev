'use strict';

/**
 * @ngdoc function
 * @name week10ControllerAsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the week10ControllerAsApp
 */
angular.module('week10ControllerAsApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
