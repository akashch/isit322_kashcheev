'use strict';

/**
 * @ngdoc function
 * @name week10ControllerAsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the week10ControllerAsApp
 */
angular.module('week10ControllerAsApp')
  .controller('MarieController', function (curleFactory) {
    var marieController = this;
    marieController.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    marieController.description = "Marie";

    marieController.details = curleFactory.marieDetails;

  });
