'use strict';

/**
 * @ngdoc overview
 * @name week10ControllerAsApp
 * @description
 * # week10ControllerAsApp
 *
 * Main module of the application.
 */
angular
  .module('week10ControllerAsApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/marie', {
        templateUrl: 'views/marie.html',
        controller: 'MarieController',
        controllerAs: 'marieController'
      })
      .when('/perrie', {
        templateUrl: 'views/perrie.html',
        controller: 'PerrieController',
        controllerAs: 'perrieController'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
