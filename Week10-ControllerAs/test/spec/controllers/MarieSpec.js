'use strict';

describe('Controller: AboutCtrl', function () {

  // load the controller's module
  beforeEach(module('week10ControllerAsApp'));

  var marieController,
    scope;




  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    marieController = $controller('MarieController', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(marieController.awesomeThings.length).toBe(3);
  });

  it('should find a marie description', function () {
    expect(marieController.description).toBe("Marie");
  });

  it('should find maries birth year', function () {
    expect(marieController.details.year).toBe("1867");
  });

});
