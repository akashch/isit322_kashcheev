'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('week10ControllerAsApp'));

  var perrieController,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    perrieController = $controller('PerrieController', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(perrieController.awesomeThings.length).toBe(3);
  });

  it('should show that pierre has description', function () {
    expect(perrieController.description).toBe("Pierre");
  });

});
