/**
 * Created by Andrey Kashcheev on 2/15/15.
 */
'use strict';

var mod = angular.module("week06AngularChartCensusApp");

mod.factory("chartService", function($q) {

  var exports = {};

  var arrOfTop5 = [];

  var indexOfCriterion;
  var indexOfTitle;

  exports.findTopFive = function(data, inputParameters){
    //console.log("findTopFive called");//debug

    arrOfTop5 = [];
    indexOfCriterion = inputParameters.indexOfCriterion;
    indexOfTitle = inputParameters.indexOfTile;

    var defers = $q.defer();
    data.shift();
    sortAndSelectItems(data);

    createChartSpecificData(arrOfTop5, defers);

    return defers.promise;
  };


  function createChartSpecificData(arrOfTop5, defers){
    //console.log("createChartSpecificData called");//debug

    var tempArr = arrOfTop5.slice();
    arrOfTop5 = [];

    tempArr.forEach(function(state){

      arrOfTop5.push({c : [
        {v: state[indexOfTitle]},
        {v: parseInt(state[indexOfCriterion])}
      ]});

      defers.resolve(arrOfTop5);
    });


  }


  function sortAndSelectItems(data){

    data.sort(comparator);
    arrOfTop5 = data.slice(0, 5);

  }





  function comparator(state01, state02) {

    if (parseInt(state01[indexOfCriterion]) <  parseInt(state02[indexOfCriterion])) {
      return 1;
    }
    if (parseInt(state01[indexOfCriterion]) >  parseInt(state02[indexOfCriterion])) {
      return -1;
    }

    return 0;
  }

  return exports;

});
