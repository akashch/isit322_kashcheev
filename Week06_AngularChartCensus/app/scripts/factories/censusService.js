/**
 * Created by Andrey Kashcheev on 2/10/15.
 */
'use strict';

var mod = angular.module("week06AngularChartCensusApp");

mod.factory("censusService", function($q, $http) {

  var exports = {};

  var token = "4032b75baba1a21e299c6460e95ea4a11c248ed3";

  var censusService = {
    "url": "http://api.census.gov/data/2010/sf1",
    "params": [
      "key",
      "get",
      "for",
      "in"
    ]
  };

  var parameters  = {
    "key": token,
    "categories" : {
      "total": "P0010001",
      "whites": "P0080003",
      "blacks": "P0080004",
      "asians": "P0080006",
      "houseUnits": "H00010001",
      "occupiedHouseUnits": "H0100001",
      "NAME":"NAME"
    },
    "geo": {
      "state" : "state",
      "country" : "country",
      "zip" : "zip+code+tabulation+area"
    }
  };

  exports.races = ["total", "whites", "blacks", "asians"];

  var API_DATA = {"serviceData" : censusService, "serviceParameters" : parameters};




  exports.getStatesData = function(inputParameters) {

    var defers = $q.defer();

    queryCensus(buildUrl(API_DATA, inputParameters), function(data){
      defers.resolve(data);
    });

    return defers.promise;
  };


  function buildUrl(data, inputParameters){

    var url = data.serviceData.url + "?";

    for(var i = 0; i < data.serviceData.params.length; i++){



      switch(i){
        case 0:
          url += data.serviceData.params[i] + "=";
          url += data.serviceParameters.key + "&";

          break;
        case 1:
          url += data.serviceData.params[i] + "=";
          for(var j = 0; j < inputParameters.categories.length; j++){
            j === inputParameters.categories.length - 1 ? url += data.serviceParameters.categories[inputParameters.categories[j]] + "&" : url += data.serviceParameters.categories[inputParameters.categories[j]] + ",";
          }

          break;
        case 2:

          url += data.serviceData.params[i] + "=";
          inputParameters.forOpr02 ? url += data.serviceParameters.geo[inputParameters.forOpr01] + ":" + inputParameters.forOpr02 : url += data.serviceParameters.geo[inputParameters.forOpr01];

          break;
        case 3:

          if(inputParameters.inOpr01 !== null) {
            url += "&" + data.serviceData.params[i] + "=";
            url += data.serviceParameters.geo[inputParameters.inOpr01] + ":" + inputParameters.inOpr02;
          }

          break;

      }
    }
    //console.log(url);
    return url;
  }



  function queryCensus(queryUrl, callback){
    //console.log(queryUrl);//debug

    $http.get(queryUrl).success(function(response){
      callback(response);
    }).error(function(err){
      console.log("ERROR");//debug
    });

  }

  return exports;

});
