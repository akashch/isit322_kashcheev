'use strict';

/**
 * @ngdoc overview
 * @name week06AngularChartCensusApp
 * @description
 * # week06AngularChartCensusApp
 *
 * Main module of the application.
 */
angular
  .module('week06AngularChartCensusApp', [
    'googlechart',
    'ui.bootstrap',
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: "views/main.html",
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/states', {
        templateUrl: 'views/states.html',
        controller: 'StatesCtrl'
      })
      .when('/zips', {
        templateUrl: 'views/zips.html',
        controller: 'ZipsCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
