/**
 * Created by Andrey Kashcheev on 2/10/15.
 */
'use strict';

/**
 * @ngdoc function
 * @name week06AngularChartCensusApp.controller:StatesCtrl
 * @description
 * # StatesCtrl
 * Controller of the week06AngularChartCensusApp
 */


angular.module('week06AngularChartCensusApp')
  .controller('StatesCtrl', function ($scope, censusService, chartService) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];


    var states = [];

    var inputParameters= {
      "categories" : ["NAME"],
      "forOpr01" : "state",
      "forOpr02" : null,
      "inOpr01" : null,
      "inOpr02" : null
    };

    censusService.getStatesData(inputParameters).then(function(data) {
      $scope.states = data;
      $scope.races = censusService.races;
    });




    $scope.search = function(){

      var inputParameters= {
        "categories" : ["total"],
        "forOpr01" : "state",
        "forOpr02" : $scope.userSelection,
        "inOpr01" : null,
        "inOpr02" : null
      };

      censusService.getStatesData(inputParameters).then(function(data) {

        $scope.pop = data[1][0];

      });

    };


    $scope.searchForRace = function(){


      var inputParameters= {
        "categories" : [$scope.userSelection2],
        "forOpr01" : "state",
        "forOpr02" : $scope.userSelection,
        "inOpr01" : null,
        "inOpr02" : null
      };

      censusService.getStatesData(inputParameters).then(function(data) {
        $scope.pop = data[1][0];
      });

    };

    $scope.topFive = function(){

      var inputParameters= {
        "categories" : ["NAME", "total"],
        "forOpr01" : "state",
        "forOpr02" : null,
        "inOpr01" : null,
        "inOpr02" : null,
        "indexOfTile" : 0,
        "indexOfCriterion" : 1
      };


      censusService.getStatesData(inputParameters).then(function(data){

        chartService.findTopFive(data, inputParameters).then(function(topFive){

          $scope.topFive = topFive;
          drawGraph(topFive);

        });
      });


    };



    function drawGraph(topFive){

      $scope.chartObject = {};

      $scope.chartObject.data = {"cols": [
        {id: "t", label: "States", type: "string"},
        {id: "s", label: "Population", type: "number"}
      ], "rows": topFive};

      $scope.chartSelect = {
        "type": "select",
        "name": "Service",
        "value": "PieChart",
        "values": [ "PieChart", "BarChart", "ColumnChart",
          "AreaChart", "LineChart", "Table"]
      };


      $scope.chartObject.type = $scope.chartSelect.value;

      $scope.chartObject.options = {
        'title': 'Top 5 Most Populated States'
      }

    }



    $scope.chartTypeUpdate = function(){

      $scope.chartObject.type = $scope.chartSelect.value;

    };

  });
