/**
 * Created by Andrey Kashcheev on 2/10/15.
 */

'use strict';

/**
 * @ngdoc function
 * @name week06AngularChartCensusApp.controller:ZipsCtrl
 * @description
 * # ZipsCtrl
 * Controller of the week06AngularChartCensusApp
 */


angular.module('week06AngularChartCensusApp')
  .controller('ZipsCtrl', function ($scope, censusService, chartService) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    //console.log("ZipsCtrl called");//debug


    var inputParameters= {
      "categories" : ["total"],
      "forOpr01" : "zip",
      "forOpr02" : "*",
      "inOpr01" : "state",
      "inOpr02" : "53"
    };

    censusService.getStatesData(inputParameters).then(function(data) {
      //console.log(data);
      data.shift();
      $scope.states = data;
      //$scope.zips = data;
    });




    $scope.getZipPopulation = function(){
      //console.log("getZipPopulation called");
      //console.log($scope.userSelection[2]);

      var inputParameters= {
        "categories" : ["total"],
        "forOpr01" : "zip",
        "forOpr02" : $scope.userSelection[2],
        "inOpr01" : "state",
        "inOpr02" : "53"
      };

      censusService.getStatesData(inputParameters).then(function(data) {
        $scope.pop = data[1][0];
      });

    };


    $scope.topFive = function(){

      //console.log("topFive called");//debug

      var inputParameters= {
        "categories" : ["total"],
        "forOpr01" : "zip",
        "forOpr02" : "*",
        "inOpr01" : "state",
        "inOpr02" : "53",
        "indexOfTile" : 2,
        "indexOfCriterion" : 0
      };

      censusService.getStatesData(inputParameters).then(function(data){
        //console.log(data);
        console.log("debug");
        console.log(data);
        chartService.findTopFive(data, inputParameters).then(function(topFive){

          //console.log(topFive);//debug
          $scope.topFive = topFive;
          drawGraph(topFive);

        });
      });


    };




    function drawGraph(topFive){

      $scope.chartObject = {};

      $scope.chartObject.data = {"cols": [
        {id: "t", label: "States", type: "string"},
        {id: "s", label: "Population", type: "number"}
      ], "rows": topFive};

      $scope.chartSelect = {
        "type": "select",
        "name": "Service",
        "value": "PieChart",
        "values": [ "PieChart", "BarChart", "ColumnChart",
          "AreaChart", "LineChart", "Table"]
      };


      $scope.chartObject.type = $scope.chartSelect.value;

      $scope.chartObject.options = {
        'title': 'Top 5 Most Populated States'
      }

    }



    $scope.chartTypeUpdate = function(){

      $scope.chartObject.type = $scope.chartSelect.value;

    };


  });
