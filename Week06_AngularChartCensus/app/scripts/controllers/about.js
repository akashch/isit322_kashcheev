'use strict';

/**
 * @ngdoc function
 * @name week06AngularChartCensusApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the week06AngularChartCensusApp
 */
angular.module('week06AngularChartCensusApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];


    console.log("AboutCtrl called");//debug
  });
