/**
 * Created by Andrey Kashcheev on 2/16/15.
 */

'use strict';

describe('Controller: StatesCtrl', function () {


  // load the controller's module
  beforeEach(module('week06AngularChartCensusApp'));


  var StatesCtrl, scope, responsesData, urls, methodsName;
  var $http, $httpBackend;




  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StatesCtrl = $controller('StatesCtrl', {
      $scope: scope
    });

    urls = [
      "http://api.census.gov/data/2010/sf1?key=4032b75baba1a21e299c6460e95ea4a11c248ed3&get=NAME&for=state",
      "http://api.census.gov/data/2010/sf1?key=4032b75baba1a21e299c6460e95ea4a11c248ed3&get=P0010001&for=state:01",
      "http://api.census.gov/data/2010/sf1?key=4032b75baba1a21e299c6460e95ea4a11c248ed3&get=P0080003&for=state:01",
      "http://api.census.gov/data/2010/sf1?key=4032b75baba1a21e299c6460e95ea4a11c248ed3&get=NAME,P0010001&for=state",
      "http://api.census.gov/data/2010/sf1?key=4032b75baba1a21e299c6460e95ea4a11c248ed3&get=P0010001&for=state:*"
    ];

    methodsName = [
      "search",
      "getStatesData",
      "searchForRace",
      "topFive"
    ];

    responsesData = {

      allStates : [["NAME","state"],["Alabama","01"],["Alaska","02"],["Arizona","04"],["Arkansas","05"],["California","06"],["Colorado","08"],["Connecticut","09"],["Delaware","10"],["District of Columbia","11"],["Florida","12"],["Georgia","13"],["Hawaii","15"],["Idaho","16"],["Illinois","17"],["Indiana","18"],["Iowa","19"], ["Kansas","20"], ["Kentucky","21"], ["Louisiana","22"], ["Maine","23"], ["Maryland","24"], ["Massachusetts","25"], ["Michigan","26"], ["Minnesota","27"], ["Mississippi","28"], ["Missouri","29"], ["Montana","30"], ["Nebraska","31"], ["Nevada","32"], ["New Hampshire","33"], ["New Jersey","34"], ["New Mexico","35"], ["New York","36"], ["North Carolina","37"], ["North Dakota","38"], ["Ohio","39"], ["Oklahoma","40"], ["Oregon","41"], ["Pennsylvania","42"], ["Rhode Island","44"], ["South Carolina","45"], ["South Dakota","46"], ["Tennessee","47"], ["Texas","48"], ["Utah","49"], ["Vermont","50"], ["Virginia","51"], ["Washington","53"], ["West Virginia","54"], ["Wisconsin","55"], ["Wyoming","56"], ["Puerto Rico","72"]],
      oneStateData : [["P0010001","state"], ["4779736","01"]],
      whitesPop : [["P0080003","state"],["3275394","01"]]

    };

    }));


  beforeEach(inject(function(_$http_, _$httpBackend_) {
    $http = _$http_;
    $httpBackend = _$httpBackend_;
    $httpBackend.whenGET(urls[0]).respond(responsesData.allStates);
    $httpBackend.flush();
  }));

  afterEach(function() {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });


  function getData(inputParameters) {

    $httpBackend.whenGET(urls[inputParameters.urlIndex]).respond(responsesData[inputParameters.responseData]);

    scope.userSelection = inputParameters.stateNumber;
    scope.userSelection2 = inputParameters.category;

    scope[methodsName[inputParameters.methodIndex]]();

    $httpBackend.flush();
  }

  it('proves that we can run test for StatesCtrl', function () {
    expect(true).toBe(true);
  });

  it('should get Alabama state population', function() {

    var inputParameters = {
      urlIndex : 1,
      methodIndex : 0,
      stateNumber : "01",
      category : "total",
      responseData : "oneStateData"
    };

    getData(inputParameters);

    expect(scope.pop).toEqual("4779736");
  });


  it('should get white population of Alabama state ', function() {

    var inputParameters = {
      urlIndex : 2,
      methodIndex : 2,
      stateNumber : "01",
      category : "whites",
      responseData : "whitesPop"
    };

    getData(inputParameters);


    expect(scope.pop).toEqual("3275394");
  });

  it('should get data for five states ', function() {

    var inputParameters = {
      urlIndex : 3,
      methodIndex : 3,
      stateNumber : null,
      category : null,
      responseData : "allStates"
    };

    getData(inputParameters);

    expect(scope.topFive.length).toEqual(5);
  });


});
