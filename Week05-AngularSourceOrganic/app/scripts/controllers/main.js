'use strict';

/**
 * @ngdoc function
 * @name week05AngularSourceOrganicApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the week05AngularSourceOrganicApp
 */
angular.module('week05AngularSourceOrganicApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];


    console.log('bootstrap setup');

    $scope.singleModel = 0;

    $scope.radioModel = 'Middle';

    $scope.checkModel = {
      left: false,
      middle: true,
      right: false
    };

  });


