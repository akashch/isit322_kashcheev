'use strict';

/**
 * @ngdoc function
 * @name week05AngularSourceOrganicApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the week05AngularSourceOrganicApp
 */
angular.module('week05AngularSourceOrganicApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.chartObject = {};


    $scope.onions = [
      {v: "Onions"},
      {v: 30}
    ];

    $scope.chartObject.data = {"cols": [
      {id: "t", label: "Topping", type: "string"},
      {id: "s", label: "Slices", type: "number"}
    ], "rows": [
      {c: [
        {v: "Mushrooms"},
        {v: 3}
      ]},
      {c: $scope.onions},
      {c: [
        {v: "Olives"},
        {v: 31}
      ]},
      {c: [
        {v: "Zucchini"},
        {v: 1}
      ]},
      {c: [
        {v: "Pepperoni"},
        {v: 2}
      ]}
    ]};


    // $routeParams.chartType == BarChart or PieChart or ColumnChart...
    $scope.chartObject.type = 'PieChart';// $routeParams.chartType;
    $scope.chartObject.options = {
      'title': 'How Much Pizza I Ate Last Night'
    }

  });
