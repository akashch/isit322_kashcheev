/**
 * Created by andrey.kashcheev on 2/21/2015.
 */
'use_strict';


var fs = require('fs');

var splitChar = "\n",
    csvFileData = '',
    rawCsv,
    fileName;

function Convert(fileNameInit){
    fileName = fileNameInit;

}


Convert.prototype.getMeetingNames = function(){
    var tempArr = [];
    csvFileData.map(function(meeting){
        tempArr.push(meeting.split(",")[4]);
    });
    return tempArr;
};


function readFile(){
    rawCsv = fs.readFileSync(fileName,'utf8');
    csvFileData = rawCsv.toString().trim().split(splitChar);
    return csvFileData;
}


function removeTitles(){
    csvFileData.shift();
}

Convert.prototype.run = function(){
    csvFileData = readFile();
    removeTitles();
    return csvFileData;
};

exports.ConvertToJson = Convert;