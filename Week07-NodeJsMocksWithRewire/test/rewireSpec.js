/**
 * Created by andrey.kashcheev on 2/21/2015.
 */
'use strict';


var chai = require('chai');
var expect = chai.expect;
var rewire = require("rewire");
var parseCsv = rewire('../source/convert.js');


describe("Json Tests", function () {
    'use strict';

    var convertToJson;

    var fsMock = {
        readFileSync: function (path, encoding) {
            return 'meeting_id, day, time, town, meeting_name\n' +
                '1,Sunday,06:00:00,Bellevue,Function Kings\n' +
                '2,Sunday,07:00:00,Seattle,Wayward Commas\n' +
                '3,Monday,07:00:00,Bellevue,Syntax Ships';
        }
    };

    parseCsv.__set__("fs", fsMock);

    beforeEach(function() {
        convertToJson = new parseCsv.ConvertToJson('Data/IsitMeetings.csv');
    });

    it("proves that true is true", function () {
        expect(true).to.equal(true);
    });

    it('proves fileName is Data/IsitMeetings.csv', function() {

        var fileName = parseCsv.__get__("fileName");
        expect(fileName).to.equal('Data/IsitMeetings.csv');
    });

    it("can read file data in format we expect", function() {
        convertToJson.run();
        var rawCsv = parseCsv.__get__("rawCsv");
        expect(rawCsv.length).to.equal(164);
    });

    it("proves we have ten records in the json object", function() {
        var json = convertToJson.run();
        expect(json.length).to.equal(3);
    });

    it("proves we can get meeting names", function() {
        convertToJson.run();
        var meetings = convertToJson.getMeetingNames();
        expect(meetings[0]).to.equal('Function Kings');
        expect(meetings[1]).to.equal('Wayward Commas');
        expect(meetings[2]).to.equal('Syntax Ships');
    });

});