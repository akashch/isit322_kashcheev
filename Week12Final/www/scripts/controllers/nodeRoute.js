/**
 * Created by Andrey Kashcheev on 3/22/15.
 */
'use strict';




angular.module('week10AngularControllerAsUnitTestsApp')
  .controller('NodeRouteCtrl', function (nodeRouteService) {

    var nodeRouteCtrl = this;

    nodeRouteCtrl.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    nodeRouteService.getStatesData().then(function(data) {
      data.shift();
      nodeRouteCtrl.states = data;
      nodeRouteCtrl.races = nodeRouteService.races;
    });

    nodeRouteService.getZipsData().then(function(data) {
      data.shift();
      nodeRouteCtrl.zips = data;
    });

    nodeRouteCtrl.search = function(){

      var data = {
        state: nodeRouteCtrl.userSelection
      };

      nodeRouteService.getOneStateData(data).then(function(data) {

        nodeRouteCtrl.statePop = data[1][0];

      });

    };


    nodeRouteCtrl.searchForRace = function(){

      var data = {
        state: nodeRouteCtrl.userSelection,
        race : nodeRouteCtrl.userSelection2
      };

      nodeRouteService.getOneStateData(data).then(function(data) {

        nodeRouteCtrl.statePop = data[1][0];

      });

    };

    nodeRouteCtrl.getZipPopulation = function(){

      var data = {
        zip: nodeRouteCtrl.userSelection3
      };

      nodeRouteService.getOneZip(data).then(function(data) {

        nodeRouteCtrl.zipPop = data[1][0];

      });

    };



  });

