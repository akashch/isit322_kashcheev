/**
 * Created by Andrey Kashcheev on 3/23/15.
 */
angular.module('week10AngularControllerAsUnitTestsApp')
    .controller('PluginsCtrl', function (cordovaAppService) {

        var pluginsCtrl = this;

        pluginsCtrl.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ];


        cordovaAppService.app.initialize();

    });