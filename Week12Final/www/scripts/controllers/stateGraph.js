/**
 * Created by Andrey Kashcheev on 2/17/15.
 */
"use strict";


angular.module("week10AngularControllerAsUnitTestsApp")
  .controller("StateGraphCtrl", function (censusService, chartService) {

    var stateGraphCtrl = this;

    stateGraphCtrl.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];

    var _sliceEndIndex,
      _sliceStartIndex;


    stateGraphCtrl.graphItems = function(sliceStartIndex, sliceEndIndex){

      _sliceEndIndex = sliceEndIndex;
      _sliceStartIndex = sliceStartIndex;


      var inputParameters= {
        "categories" : ["NAME", "total"],
        "forOpr01" : "state",
        "forOpr02" : null,
        "inOpr01" : null,
        "inOpr02" : null,
        "indexOfTile" : 0,
        "indexOfCriterion" : 1,
        "sliceStartIndex" : sliceStartIndex,
        "sliceEndIndex" : sliceEndIndex
      };


      censusService.getStatesData(inputParameters).then(function(data){

        chartService.findItems(data, inputParameters).then(function(items){

          stateGraphCtrl.items = items;
          drawGraph(items);

        });
      });

    };




    function drawGraph(items){

      var title;

      if(_sliceEndIndex > 0){
        title = "Top " + _sliceEndIndex + " Most Populated States";
      }else{
        title = "Top " + Math.abs(_sliceEndIndex) + " Less Populated States";
      }

      stateGraphCtrl.chartObject = {};

      stateGraphCtrl.chartObject.data = {"cols": [
        {id: "t", label: "States", type: "string"},
        {id: "s", label: "Population", type: "number"}
      ], "rows": items};

      stateGraphCtrl.chartSelect = {
        "type": "select",
        "name": "Service",
        "value": "PieChart",
        "values": [ "PieChart", "BarChart", "ColumnChart",
          "AreaChart", "LineChart", "Table"]
      };


      stateGraphCtrl.chartObject.type = stateGraphCtrl.chartSelect.value;

      stateGraphCtrl.chartObject.options = {
        'title': title
      };

    }



    stateGraphCtrl.chartTypeUpdate = function(){

      stateGraphCtrl.chartObject.type = stateGraphCtrl.chartSelect.value;

    };

  });
