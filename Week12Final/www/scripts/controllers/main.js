'use strict';

/**
 * @ngdoc function
 * @name week06AngularChartCensusApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the week06AngularChartCensusApp
 */
angular.module('week10AngularControllerAsUnitTestsApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
