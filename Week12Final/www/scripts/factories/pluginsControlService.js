/**
 * Created by Andrey Kashcheev on 3/23/15.
 */

var mod = angular.module("utils");

mod.factory("pluginsControlService", function(geoLocationService, networkInformationService) {

    var exports = {};

    exports = (function(){


        var app;

        function Control(theApp){
            console.log("Control constructor called");
            app = theApp;

            init();
        }

        function init(){
            console.log("Control init called");

            new networkInformationService();


            /* Geo location */
            app.geolocation = false;
            $('#runGeo').click(runGeo);
            if(navigator.geolocation) {
                app.geolocation = navigator.geolocation;
                exports.showMessage('adding geolocation from navigator');
            }
            /***************/
        }

        function runGeo(){
            console.log("Control runGeo called");
            var geo = new geoLocationService(exports, app);
            geo.run();
        }

        Control.showMessage = function(message){
            $('#debug').append('<li>' + message + '</li>');
        };

        return Control;

    })();


    return exports;

});
