/**
 * Created by Andrey Kashcheev on 3/23/15.
 */
var mod = angular.module("utils");

mod.factory("cordovaAppService", function(pluginsControlService) {

    var exports = {};

    exports.app = {
        initialize: function() {
            console.log("initialize called");
            this.bindEvents();
            new pluginsControlService(exports.app);

        },
        bindEvents: function() {

            document.addEventListener('deviceready', this.onDeviceReady, false);

        },
        onDeviceReady: function() {
            exports.app.receivedEvent('deviceready');

        },
        receivedEvent: function(id) {
            var parentElement = document.getElementById(id);
            var listeningElement = parentElement.querySelector('.listening');
            var receivedElement = parentElement.querySelector('.received');
            listeningElement.setAttribute('style', 'display:none;');
            receivedElement.setAttribute('style', 'display:block;');

            console.log('Received Event: ' + id);

        }
    };

    return exports;

});