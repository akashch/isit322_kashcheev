/**
 * Created by Andrey Kashcheev on 2/15/15.
 */
"use strict";

var mod = angular.module("utils");

mod.factory("chartService", function($q) {

  var exports = {};

  var arrOfTop5 = [];

  var indexOfCriterion;
  var indexOfTitle;
  var sliceEndIndex,
      sliceStartIndex;

  exports.findItems = function(data, inputParameters){

    arrOfTop5 = [];
    indexOfCriterion = inputParameters.indexOfCriterion;
    indexOfTitle = inputParameters.indexOfTile;
    sliceEndIndex = inputParameters.sliceEndIndex;
    sliceStartIndex = inputParameters.sliceStartIndex;

    var defers = $q.defer();
    data.shift();
    sortAndSelectItems(data);

    createChartSpecificData(arrOfTop5, defers);

    return defers.promise;
  };


  function createChartSpecificData(arrOfTop5, defers){

    var tempArr = arrOfTop5.slice();
    arrOfTop5 = [];

    tempArr.forEach(function(state){
      arrOfTop5.push({c : [
        {v: state[indexOfTitle]},
        {v: parseInt(state[indexOfCriterion])}
      ]});


    });

      defers.resolve(arrOfTop5);
  }


  function sortAndSelectItems(data){

    data = data.sort(comparator).filter(function(item){
      return (item[0] != 0);
    });

    sliceEndIndex > 0 && sliceEndIndex >  sliceStartIndex ?  arrOfTop5 = data.slice(sliceStartIndex, sliceEndIndex) : arrOfTop5 = data.slice(sliceEndIndex);

  }


  function comparator(state01, state02) {

    if (parseInt(state01[indexOfCriterion]) <  parseInt(state02[indexOfCriterion])) {
      return 1;
    }
    if (parseInt(state01[indexOfCriterion]) >  parseInt(state02[indexOfCriterion])) {
      return -1;
    }

    return 0;
  }

  return exports;

});
