/**
 * Created by Andrey Kashcheeev on 3/23/15.
 */
var mod = angular.module("utils");

mod.factory("geoLocationService", function() {

    var exports = {};

    exports = (function() {


        var pluginsConrtol, app;

        function Geo(theControl, theApp) {
            pluginsConrtol = theControl;
            theControl.showMessage('In Geo');

            app = theApp;
        }

        function showMessage(message) {
            $("#geo").append('<li>' + message + '</li>');
        }

        var onSuccess = function(position) {
            showMessage('Latitude: ' + position.coords.latitude);
            showMessage('Longitude: ' + position.coords.longitude);
            showMessage('Altitude: '          + position.coords.altitude);
            showMessage('Accuracy: '          + position.coords.accuracy);
            showMessage('Altitude Accuracy: ' + position.coords.altitudeAccuracy);
            showMessage('Heading: '           + position.coords.heading);
            showMessage('Speed: '             + position.coords.speed);
            showMessage('Timestamp: '         + position.timestamp);
        };

        function onError(error) {
            pluginsConrtol.showMessage('code: '    + error.code + ' message: ' + error.message);
        }

        Geo.prototype.run = function() {
            if(app.geolocation) {
                var locationService = app.geolocation;

                pluginsConrtol.showMessage('Using native HTML5 geolocation');
            }
            else {
                var locationService = navigator.geolocation;
                pluginsConrtol.showMessage('Using cordova geolocation plugin');
            }
            locationService.getCurrentPosition(onSuccess, onError, { enableHighAccuracy: true, timeout: 5*1000, maximumAge: 0 });
        };


        return Geo;

    })();

    return exports;

});