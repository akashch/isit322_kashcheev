/**
 * Created by Andrey Kashcheev on 3/22/15.
 */
'use strict';

var mod = angular.module("utils");

mod.factory("nodeRouteService", function($q, $http) {

  var exports = {};


  exports.races = ["total", "whites", "blacks", "asians"];


  exports.getStatesData = function(){

    var defers = $q.defer();

    var url = "http://54.148.86.85:30026/censusService/getStates";

    queryNodeRoute(url, function(respData){

      defers.resolve(respData);

    });

    return defers.promise;
  };


  exports.getOneStateData = function(data){

    var defers = $q.defer();

    if(data.race){
      var url = "http://54.148.86.85:30026/censusService/getOneStateData?race=" + data.race + "&state=" + data.state;
    }else{
      var url = "http://54.148.86.85:30026/censusService/getOneStateData?state=" + data.state;
    }


    queryNodeRoute(url, function(respData){

      defers.resolve(respData);

    });

    return defers.promise;
  };

  exports.getZipsData = function(){

    var defers = $q.defer();

    var url = "http://54.148.86.85:30026/censusService/getZips";

    queryNodeRoute(url, function(respData){

      defers.resolve(respData);

    });

    return defers.promise;
  };

  exports.getOneZip = function(data){

    var defers = $q.defer();

    var url = "http://54.148.86.85:30026/censusService/getOneZip?zip=" + data.zip;

    queryNodeRoute(url, function(respData){

      defers.resolve(respData);

    });

    return defers.promise;
  };

  function queryNodeRoute(url, callback){

    var req = {
      method: 'GET',
      url: url
    };

    $http(req).success(function(response){
      callback(response);
    }).error(function(err){
      console.log(err);//debug
    });

  }


  return exports;
});
