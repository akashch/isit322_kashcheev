/**
 * Created by Andrey Kashcheev on 2/23/15.
 */
"use strict";

var mod = angular.module("utils");


mod.factory("urlService", function() {

  var exports = {};


  var token = "4032b75baba1a21e299c6460e95ea4a11c248ed3";

  var censusService = {
    "url": "http://api.census.gov/data/2010/sf1",
    "params": [
      "key",
      "get",
      "for",
      "in"
    ]
  };

  var parameters  = {
    "key": token,
    "categories" : {
      "total": "P0010001",
      "whites": "P0080003",
      "blacks": "P0080004",
      "asians": "P0080006",
      "houseUnits": "H00010001",
      "occupiedHouseUnits": "H0100001",
      "NAME":"NAME"
    },
    "geo": {
      "state" : "state",
      "country" : "country",
      "zip" : "zip+code+tabulation+area"
    }
  };


  var API_DATA = {"serviceData" : censusService, "serviceParameters" : parameters};


  exports.buildUrl = function(inputParameters){

    var url = API_DATA.serviceData.url + "?";

    for(var i = 0; i < API_DATA.serviceData.params.length; i++){



      switch(i){
        case 0:
          url += API_DATA.serviceData.params[i] + "=";
          url += API_DATA.serviceParameters.key + "&";

          break;
        case 1:
          url += API_DATA.serviceData.params[i] + "=";
          for(var j = 0; j < inputParameters.categories.length; j++){
            j === inputParameters.categories.length - 1 ? url += API_DATA.serviceParameters.categories[inputParameters.categories[j]] + "&" : url += API_DATA.serviceParameters.categories[inputParameters.categories[j]] + ",";
          }

          break;
        case 2:

          url += API_DATA.serviceData.params[i] + "=";
          inputParameters.forOpr02 ? url += API_DATA.serviceParameters.geo[inputParameters.forOpr01] + ":" + inputParameters.forOpr02 : url += API_DATA.serviceParameters.geo[inputParameters.forOpr01];

          break;
        case 3:

          if(inputParameters.inOpr01 !== null) {
            url += "&" + API_DATA.serviceData.params[i] + "=";
            url += API_DATA.serviceParameters.geo[inputParameters.inOpr01] + ":" + inputParameters.inOpr02;
          }

          break;

      }
    }
    console.log(url);
    return url;
  };

  return exports;

});
