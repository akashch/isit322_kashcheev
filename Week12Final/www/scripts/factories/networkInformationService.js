/**
 * Created by Andrey Kashcheev on 3/23/15.
 */
var mod = angular.module("utils");

mod.factory("networkInformationService", function() {

    var exports = (function(){


        function NetworkInfo(){

            init();
        }

        function init(){
            $("#runNetworkInfo").click(checkConnection);

        }

        function checkConnection() {
            var networkState = navigator.connection.type;

            var states = {};
            states[Connection.UNKNOWN]  = 'Unknown connection';
            states[Connection.ETHERNET] = 'Ethernet connection';
            states[Connection.WIFI]     = 'WiFi connection';
            states[Connection.CELL_2G]  = 'Cell 2G connection';
            states[Connection.CELL_3G]  = 'Cell 3G connection';
            states[Connection.CELL_4G]  = 'Cell 4G connection';
            states[Connection.CELL]     = 'Cell generic connection';
            states[Connection.NONE]     = 'No network connection';

            console.log(states[networkState]);
            $("#status").text(states[networkState]);
        }


        return NetworkInfo;
    })();


    return exports;
});