/**
 * Created by bcuser on 1/26/15.
 */
require.config({
    baseUrl : '.',
    paths : {
        //"jquery" : 'http://code.jquery.com/jquery-1.11.0.min'\
        "jquery" : "js/lib/jquery",
        "Control" : "js/controls/Control",
        "ApiControl" : "js/controls/ApiControl"
    },
    shim : {

    }
});

require(['jquery', 'Control'], function(jq, Control) {
                        'use strict';

        $(document).ready(function() {
            var control = new Control();

        });

});