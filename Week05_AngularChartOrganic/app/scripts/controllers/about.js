'use strict';

/**
 * @ngdoc function
 * @name week05AngularChartOrganicApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the week05AngularChartOrganicApp
 */
angular.module('week05AngularChartOrganicApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];


    $scope.chartObject = {};


    $scope.danemark = [
      {v: "Danemark"},
      {v: 25}
    ];

    $scope.austria = [
      {v: "Austria"},
      {v: 35}
    ];


    $scope.sweden = [
      {v: "Sweden"},
      {v: 20}
    ];


    $scope.russia = [
      {v: "Russia"},
      {v: 34}
    ];

    $scope.germany = [
      {v: "Germany"},
      {v: 40}
    ];

    $scope.france = [
      {v: "France"},
      {v: 50}
    ];

    $scope.chartObject.data = {"cols": [
      {id: "t", label: "Countries", type: "string"},
      {id: "s", label: "Food", type: "number"}
    ], "rows": [
      {c: $scope.austria},
      {c: $scope.danemark},
      {c: $scope.sweden},
      {c: $scope.russia},
      {c: $scope.france},
      {c: $scope.germany}
    ]};

    $scope.chartSelect = {
      "type": "select",
      "name": "Service",
      "value": "PieChart",
      "values": [ "PieChart", "BarChart", "ColumnChart",
        "AreaChart", "LineChart", "Table"]
    };


    $scope.chartObject.type = $scope.chartSelect.value;

    $scope.chartObject.options = {
      'title': 'Top consumers of organic food in Euro'
    }





    $scope.chartTypeUpdate = function(){

      $scope.chartObject.type = $scope.chartSelect.value;

    };

  });
