'use strict';

/**
 * @ngdoc overview
 * @name week05AngularChartOrganicApp
 * @description
 * # week05AngularChartOrganicApp
 *
 * Main module of the application.
 */
angular
  .module('week05AngularChartOrganicApp', [
    'googlechart',
    'ui.bootstrap',
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
