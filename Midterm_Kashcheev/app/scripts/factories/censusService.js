/**
 * Created by Andrey Kashcheev on 2/10/15.
 */
'use strict';

var mod = angular.module("utils");

mod.factory("censusService", function($q, $http, urlService) {

  var exports = {};

  exports.races = ["total", "whites", "blacks", "asians"];

  exports.getStatesData = function(inputParameters) {

    var defers = $q.defer();


    queryCensus(urlService.buildUrl(inputParameters), function(data){
      defers.resolve(data);
    });

    return defers.promise;
  };



  function queryCensus(queryUrl, callback){
    //console.log(queryUrl);//debug

    $http.get(queryUrl).success(function(response){
      callback(response);
    }).error(function(err){
      console.log(err);//debug
    });

  }

  return exports;

});
