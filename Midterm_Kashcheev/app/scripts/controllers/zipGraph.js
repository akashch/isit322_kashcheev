/**
 * Created by Andrey Kashcheev on 2/17/15.
 */
"use strict";

angular.module("midtermKashcheevApp")
  .controller("ZipGraphCtrl", function ($scope, censusService, chartService) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
    var _sliceEndIndex,
        _sliceStartIndex;

$scope.graphItems = function(sliceStartIndex, sliceEndIndex){


  _sliceEndIndex = sliceEndIndex;
  _sliceStartIndex = sliceStartIndex;
  var inputParameters= {
    "categories" : ["total"],
    "forOpr01" : "zip",
    "forOpr02" : "*",
    "inOpr01" : "state",
    "inOpr02" : "53",
    "indexOfTile" : 2,
    "indexOfCriterion" : 0,
    "sliceStartIndex" : sliceStartIndex,
    "sliceEndIndex" : sliceEndIndex
  };

  censusService.getStatesData(inputParameters).then(function(data){

    chartService.findItems(data, inputParameters).then(function(selectedItems){

      $scope.items = selectedItems;
      drawGraph(selectedItems);

    });
  });

};

function drawGraph(selectedItems){


  var title;

  _sliceEndIndex > 0 ? title = "Top " + _sliceEndIndex + " Most Populated States" : title = 'Top ' + Math.abs(_sliceEndIndex) + ' Less Populated States';

  $scope.chartObject = {};

  $scope.chartObject.data = {"cols": [
    {id: "t", label: "States", type: "string"},
    {id: "s", label: "Population", type: "number"}
  ], "rows": selectedItems};

  $scope.chartSelect = {
    "type": "select",
    "name": "Service",
    "value": "PieChart",
    "values": [ "PieChart", "BarChart", "ColumnChart", "AreaChart", "LineChart", "Table"]
  };


  $scope.chartObject.type = $scope.chartSelect.value;

  $scope.chartObject.options = {
    'title': title
  };

}


$scope.chartTypeUpdate = function(){

  $scope.chartObject.type = $scope.chartSelect.value;

};
});
