/**
 * Created by Andrey Kashcheev on 2/17/15.
 */

'use strict';

/**
 * @ngdoc function
 * @name week06AngularChartCensusApp.controller:ZipsCtrl
 * @description
 * # ZipsCtrl
 * Controller of the midtermKashcheevApp
 */


angular.module('midtermKashcheevApp')
  .controller('ZipsCtrl', function ($scope, censusService) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    //console.log("ZipsCtrl called");//debug


    var inputParameters= {
      "categories" : ["total"],
      "forOpr01" : "zip",
      "forOpr02" : "*",
      "inOpr01" : "state",
      "inOpr02" : "53"
    };

    censusService.getStatesData(inputParameters).then(function(data) {
      //console.log(data);
      data.shift();
      $scope.states = data;
      //$scope.zips = data;
    });




    $scope.getZipPopulation = function(){
      //console.log("getZipPopulation called");
      //console.log($scope.userSelection[2]);

      var inputParameters= {
        "categories" : ["total"],
        "forOpr01" : "zip",
        "forOpr02" : $scope.userSelection[2],
        "inOpr01" : "state",
        "inOpr02" : "53"
      };

      censusService.getStatesData(inputParameters).then(function(data) {
        $scope.pop = data[1][0];
      });

    };


  });
