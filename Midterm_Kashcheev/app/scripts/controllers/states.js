/**
 * Created by Andrey Kashcheev on 2/17/15.
 */
'use strict';

/**
 * @ngdoc function
 * @name week06AngularChartCensusApp.controller:StatesCtrl
 * @description
 * # StatesCtrl
 * Controller of the midtermKashcheevApp
 */


angular.module('midtermKashcheevApp')
  .controller('StatesCtrl', function ($scope, censusService) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];


    var inputParameters= {
      "categories" : ["NAME"],
      "forOpr01" : "state",
      "forOpr02" : null,
      "inOpr01" : null,
      "inOpr02" : null
    };

    censusService.getStatesData(inputParameters).then(function(data) {
      $scope.states = data;
      $scope.races = censusService.races;
    });




    $scope.search = function(){

      var inputParameters= {
        "categories" : ["total"],
        "forOpr01" : "state",
        "forOpr02" : $scope.userSelection,
        "inOpr01" : null,
        "inOpr02" : null
      };

      censusService.getStatesData(inputParameters).then(function(data) {

        $scope.pop = data[1][0];

      });

    };


    $scope.searchForRace = function(){


      var inputParameters= {
        "categories" : [$scope.userSelection2],
        "forOpr01" : "state",
        "forOpr02" : $scope.userSelection,
        "inOpr01" : null,
        "inOpr02" : null
      };

      censusService.getStatesData(inputParameters).then(function(data) {
        $scope.pop = data[1][0];
      });

    };

  });
