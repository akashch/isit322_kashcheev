/**
 * Created by Andrey Kashcheev on 2/17/15.
 */
"use strict";

/**
 * @ngdoc function
 * @name week06AngularChartCensusApp.controller:StateGraphCtrl
 * @description
 * # StateGraphCtrl
 * Controller of the midtermKashcheevApp
 */


angular.module("midtermKashcheevApp")
  .controller("StateGraphCtrl", function ($scope, censusService, chartService) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];

    var _sliceEndIndex,
      _sliceStartIndex;


    $scope.graphItems = function(sliceStartIndex, sliceEndIndex){

      _sliceEndIndex = sliceEndIndex;
      _sliceStartIndex = sliceStartIndex;


      var inputParameters= {
        "categories" : ["NAME", "total"],
        "forOpr01" : "state",
        "forOpr02" : null,
        "inOpr01" : null,
        "inOpr02" : null,
        "indexOfTile" : 0,
        "indexOfCriterion" : 1,
        "sliceStartIndex" : sliceStartIndex,
        "sliceEndIndex" : sliceEndIndex
      };


      censusService.getStatesData(inputParameters).then(function(data){

        chartService.findItems(data, inputParameters).then(function(items){

          $scope.items = items;
          drawGraph(items);

        });
      });

    };




    function drawGraph(items){

      var title;

      if(_sliceEndIndex > 0){
        title = "Top " + _sliceEndIndex + " Most Populated States";
      }else{
        title = "Top " + Math.abs(_sliceEndIndex) + " Less Populated States";
      }

      $scope.chartObject = {};

      $scope.chartObject.data = {"cols": [
        {id: "t", label: "States", type: "string"},
        {id: "s", label: "Population", type: "number"}
      ], "rows": items};

      $scope.chartSelect = {
        "type": "select",
        "name": "Service",
        "value": "PieChart",
        "values": [ "PieChart", "BarChart", "ColumnChart",
          "AreaChart", "LineChart", "Table"]
      };


      $scope.chartObject.type = $scope.chartSelect.value;

      $scope.chartObject.options = {
        'title': title
      };

    }



    $scope.chartTypeUpdate = function(){

      $scope.chartObject.type = $scope.chartSelect.value;

    };

  });
