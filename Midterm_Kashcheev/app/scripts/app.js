'use strict';

/**
 * @ngdoc overview
 * @name midtermKashcheevApp
 * @description
 * # midtermKashcheevApp
 *
 * Main module of the application.
 */

angular.module("utils", []);

angular
  .module('midtermKashcheevApp', [
    "utils",
    'googlechart',
    'ui.bootstrap',
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: "views/main.html",
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/states', {
        templateUrl: 'views/states.html',
        controller: 'StatesCtrl'
      })
      .when('/stateGraph', {
        templateUrl: 'views/stateGraph.html',
        controller: 'StateGraphCtrl'
      })
      .when('/zips', {
        templateUrl: 'views/zips.html',
        controller: 'ZipsCtrl'
      })
      .when('/zipGraph', {
        templateUrl: 'views/zipGraph.html',
        controller: 'ZipGraphCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  }).controller('HeaderController', function($scope, $location)
  {
    $scope.isActive = function (viewLocation) {
      return viewLocation === $location.path();
    };
  });


