/**
 * Created by Andrey Kashcheev on 2/16/15.
 */

'use strict';

describe('Controller: ZipsCtrl', function () {

  // load the controller's module
  beforeEach(module('midtermKashcheevApp'));

  var ZipsCtrl, scope, responsesData, urls, methodsName;
  var $http, $httpBackend;



  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ZipsCtrl = $controller('ZipsCtrl', {
      $scope: scope
    });


    urls = [
        "http://api.census.gov/data/2010/sf1?key=4032b75baba1a21e299c6460e95ea4a11c248ed3&get=P0010001&for=zip+code+tabulation+area:*&in=state:53",
        "http://api.census.gov/data/2010/sf1?key=4032b75baba1a21e299c6460e95ea4a11c248ed3&get=P0010001&for=zip+code+tabulation+area:98029&in=state:53"
    ];

    methodsName = [
      "getZipPopulation"
    ];

    responsesData = {

      allZips : [["P0010001","state","zip code tabulation area"], ["31911","53","98001"], ["31647","53","98002"], ["44151","53","98003"], ["27946","53","98004"], ["17714","53","98005"], ["36364","53","98006"], ["24889","53","98007"], ["24411","53","98008"], ["5025","53","98010"], ["29212","53","98011"], ["51136","53","98012"], ["6765","53","98014"], ["10725","53","98019"], ["18304","53","98020"], ["26722","53","98021"], ["20987","53","98022"], ["47510","53","98023"], ["5650","53","98024"], ["35921","53","98026"], ["26141","53","98027"], ["20419","53","98028"], ["24348","53","98029"], ["33769","53","98030"], ["36581","53","98031"], ["33853","53","98032"], ["34338","53","98033"], ["40407","53","98034"], ["36000","53","98036"], ["26889","53","98037"], ["31171","53","98038"], ["2971","53","98039"], ["22699","53","98040"], ["43673","53","98042"], ["19943","53","98043"], ["13888","53","98045"], ["6339","53","98047"], ["322","53","98050"], ["3270","53","98051"], ["58442","53","98052"], ["18784","53","98053"], ["21904","53","98055"], ["32489","53","98056"], ["10613","53","98057"], ["41938","53","98058"], ["34463","53","98059"], ["12699","53","98065"], ["403","53","98068"], ["10624","53","98070"], ["22312","53","98072"], ["25748","53","98074"], ["20715","53","98075"], ["13585","53","98077"], ["29976","53","98087"], ["39816","53","98092"], ["10238","53","98101"], ["20756","53","98102"], ["45911","53","98103"], ["13095","53","98104"], ["43924","53","98105"], ["22873","53","98106"], ["21147","53","98107"], ["22374","53","98108"], ["20715","53","98109"], ["23025","53","98110"], ["21077","53","98112"], ["46206","53","98115"], ["22241","53","98116"], ["31365","53","98117"], ["42731","53","98118"], ["21039","53","98119"], ["12628","53","98121"], ["31454","53","98122"], ["37081","53","98125"], ["20698","53","98126"], ["44555","53","98133"], ["644","53","98134"], ["14770","53","98136"], ["26881","53","98144"], ["25922","53","98146"], ["10010","53","98148"], ["0","53","98154"], ["32778","53","98155"], ["0","53","98158"], ["141","53","98164"], ["20301","53","98166"], ["33734","53","98168"], ["0","53","98174"], ["19030","53","98177"], ["24092","53","98178"], ["23111","53","98188"], ["0","53","98195"], ["34584","53","98198"], ["19686","53","98199"], ["29582","53","98201"], ["34354","53","98203"], ["39380","53","98204"], ["12283","53","98205"], ["758","53","98207"], ["51802","53","98208"], ["634","53","98220"], ["20429","53","98221"], ["33","53","98222"], ["40815","53","98223"], ["294","53","98224"], ["46172","53","98225"], ["41235","53","98226"], ["30321","53","98229"], ["15710","53","98230"], ["3920","53","98232"], ["14871","53","98233"], ["103","53","98235"], ["5635","53","98236"], ["4025","53","98237"], ["85","53","98238"], ["6646","53","98239"], ["3061","53","98240"], ["2129","53","98241"], ["507","53","98243"], ["3001","53","98244"], ["3651","53","98245"], ["9072","53","98247"], ["22361","53","98248"], ["4561","53","98249"], ["7664","53","98250"], ["4567","53","98251"], ["8777","53","98252"], ["1837","53","98253"], ["245","53","98255"], ["420","53","98256"], ["4134","53","98257"], ["30524","53","98258"], ["5278","53","98260"], ["2383","53","98261"], ["964","53","98262"], ["76","53","98263"], ["18893","53","98264"], ["3642","53","98266"], ["534","53","98267"], ["45899","53","98270"], ["27184","53","98271"], ["27942","53","98272"], ["28717","53","98273"], ["16613","53","98274"], ["20256","53","98275"], ["450","53","98276"], ["37823","53","98277"], ["1065","53","98278"], ["608","53","98279"], ["480","53","98280"], ["1314","53","98281"], ["15661","53","98282"], ["434","53","98283"], ["24331","53","98284"], ["240","53","98286"], ["333","53","98288"], ["32714","53","98290"], ["20987","53","98292"], ["6595","53","98294"], ["2409","53","98295"], ["27956","53","98296"], ["106","53","98297"], ["1037","53","98303"], ["759","53","98304"], ["358","53","98305"], ["18703","53","98310"], ["25880","53","98311"], ["30203","53","98312"], ["3329","53","98314"], ["6054","53","98315"], ["1250","53","98320"], ["15152","53","98321"], ["702","53","98323"], ["1848","53","98325"], ["1412","53","98326"], ["8267","53","98327"], ["10433","53","98328"], ["10545","53","98329"], ["149","53","98330"], ["6261","53","98331"], ["15855","53","98332"], ["3633","53","98333"], ["24925","53","98335"], ["970","53","98336"], ["6697","53","98337"], ["24098","53","98338"], ["3326","53","98339"], ["2537","53","98340"], ["1370","53","98342"], ["554","53","98345"], ["9297","53","98346"], ["4614","53","98349"], ["460","53","98350"], ["1115","53","98351"], ["4","53","98353"], ["6976","53","98354"], ["628","53","98355"], ["2149","53","98356"], ["1414","53","98357"], ["897","53","98358"], ["4887","53","98359"], ["12221","53","98360"], ["1158","53","98361"], ["22230","53","98362"], ["13343","53","98363"], ["45","53","98364"], ["4528","53","98365"], ["34258","53","98366"], ["27693","53","98367"], ["14724","53","98368"], ["29528","53","98370"], ["20468","53","98371"], ["22475","53","98372"], ["22246","53","98373"], ["37360","53","98374"], ["26863","53","98375"], ["1998","53","98376"], ["1922","53","98377"], ["4173","53","98380"], ["371","53","98381"], ["26856","53","98382"], ["19727","53","98383"], ["459","53","98385"], ["43173","53","98387"], ["7508","53","98388"], ["10461","53","98390"], ["44309","53","98391"], ["2994","53","98392"], ["1107","53","98394"], ["662","53","98396"], ["6356","53","98402"], ["7713","53","98403"], ["32086","53","98404"], ["22851","53","98405"], ["21610","53","98406"], ["19885","53","98407"], ["18830","53","98408"], ["22989","53","98409"], ["1014","53","98416"], ["10069","53","98418"], ["1308","53","98421"], ["20151","53","98422"], ["10140","53","98424"], ["13","53","98430"], ["14584","53","98433"], ["366","53","98438"], ["4839","53","98439"], ["5371","53","98443"], ["33956","53","98444"], ["29661","53","98445"], ["10173","53","98446"], ["842","53","98447"], ["6340","53","98465"], ["26944","53","98466"], ["14728","53","98467"], ["27546","53","98498"], ["29750","53","98499"], ["38133","53","98501"], ["30491","53","98502"], ["36611","53","98503"], ["18199","53","98506"], ["28130","53","98512"], ["31975","53","98513"], ["20166","53","98516"], ["23814","53","98520"], ["3232","53","98524"], ["398","53","98526"], ["324","53","98527"], ["9983","53","98528"], ["565","53","98530"], ["24121","53","98531"], ["23596","53","98532"], ["609","53","98533"], ["445","53","98535"], ["211","53","98536"], ["2129","53","98537"], ["304","53","98538"], ["10101","53","98541"], ["656","53","98542"], ["73","53","98544"], ["2909","53","98546"], ["1436","53","98547"], ["2193","53","98548"], ["11382","53","98550"], ["508","53","98552"], ["599","53","98555"], ["3225","53","98557"], ["192","53","98558"], ["93","53","98559"], ["142","53","98560"], ["195","53","98562"], ["8001","53","98563"], ["2459","53","98564"], ["618","53","98565"], ["2519","53","98568"], ["5784","53","98569"], ["3889","53","98570"], ["483","53","98571"], ["797","53","98572"], ["228","53","98575"], ["4667","53","98576"], ["6338","53","98577"], ["12870","53","98579"], ["11099","53","98580"], ["424","53","98581"], ["611","53","98582"], ["124","53","98583"], ["36630","53","98584"], ["655","53","98585"], ["1944","53","98586"], ["942","53","98587"], ["1829","53","98588"], ["7320","53","98589"], ["356","53","98590"], ["3952","53","98591"], ["1764","53","98592"], ["1113","53","98593"], ["2917","53","98595"], ["6524","53","98596"], ["21557","53","98597"], ["2982","53","98601"], ["200","53","98602"], ["1017","53","98603"], ["34232","53","98604"], ["1225","53","98605"], ["9027","53","98606"], ["27899","53","98607"], ["2871","53","98610"], ["9133","53","98611"], ["2704","53","98612"], ["400","53","98613"], ["353","53","98614"], ["150","53","98616"], ["1042","53","98617"], ["457","53","98619"], ["7123","53","98620"], ["276","53","98621"], ["1532","53","98624"], ["6133","53","98625"], ["24523","53","98626"], ["385","53","98628"], ["8364","53","98629"], ["3243","53","98631"], ["49205","53","98632"], ["1735","53","98635"], ["1525","53","98638"], ["847","53","98639"], ["4380","53","98640"], ["5","53","98641"], ["15696","53","98642"], ["362","53","98643"], ["473","53","98644"], ["1429","53","98645"], ["404","53","98647"], ["3100","53","98648"], ["895","53","98649"], ["959","53","98650"], ["939","53","98651"], ["11858","53","98660"], ["41740","53","98661"], ["31644","53","98662"], ["14115","53","98663"], ["21771","53","98664"], ["24057","53","98665"], ["113","53","98670"], ["21220","53","98671"], ["6146","53","98672"], ["404","53","98673"], ["12029","53","98674"], ["6713","53","98675"], ["52893","53","98682"], ["30832","53","98683"], ["26968","53","98684"], ["26217","53","98685"], ["17385","53","98686"], ["40977","53","98801"], ["28719","53","98802"], ["71","53","98811"], ["4899","53","98812"], ["3176","53","98813"], ["466","53","98814"], ["7233","53","98815"], ["6394","53","98816"], ["366","53","98817"], ["258","53","98819"], ["519","53","98821"], ["1953","53","98822"], ["11055","53","98823"], ["181","53","98824"], ["6504","53","98826"], ["312","53","98827"], ["2146","53","98828"], ["564","53","98829"], ["494","53","98830"], ["3708","53","98831"], ["320","53","98832"], ["150","53","98833"], ["239","53","98834"], ["584","53","98836"], ["39722","53","98837"], ["4851","53","98840"], ["9197","53","98841"], ["1817","53","98843"], ["4740","53","98844"], ["128","53","98845"], ["1173","53","98846"], ["1920","53","98847"], ["11518","53","98848"], ["1078","53","98849"], ["1543","53","98850"], ["4014","53","98851"], ["104","53","98852"], ["23","53","98853"], ["5863","53","98855"], ["2464","53","98856"], ["4082","53","98857"], ["1521","53","98858"], ["238","53","98859"], ["259","53","98860"], ["2218","53","98862"], ["30169","53","98901"], ["46322","53","98902"], ["14517","53","98903"], ["35240","53","98908"], ["516","53","98921"], ["5468","53","98922"], ["1434","53","98923"], ["689","53","98925"], ["30239","53","98926"], ["0","53","98929"], ["15252","53","98930"], ["5032","53","98932"], ["1267","53","98933"], ["1117","53","98934"], ["4190","53","98935"], ["5872","53","98936"], ["4112","53","98937"], ["2177","53","98938"], ["167","53","98939"], ["760","53","98940"], ["919","53","98941"], ["16973","53","98942"], ["660","53","98943"], ["22014","53","98944"], ["495","53","98946"], ["2902","53","98947"], ["13225","53","98948"], ["177","53","98950"], ["13739","53","98951"], ["2330","53","98952"], ["6681","53","98953"], ["6151","53","99001"], ["4848","53","99003"], ["18376","53","99004"], ["8574","53","99005"], ["11946","53","99006"], ["641","53","99008"], ["3947","53","99009"], ["2776","53","99011"], ["1064","53","99012"], ["1785","53","99013"], ["12480","53","99016"], ["161","53","99017"], ["282","53","99018"], ["9502","53","99019"], ["82","53","99020"], ["9174","53","99021"], ["8820","53","99022"], ["529","53","99023"], ["5232","53","99025"], ["9042","53","99026"], ["6101","53","99027"], ["1289","53","99029"], ["1032","53","99030"], ["1137","53","99031"], ["691","53","99032"], ["895","53","99033"], ["209","53","99034"], ["1448","53","99036"], ["12973","53","99037"], ["87","53","99039"], ["734","53","99040"], ["1453","53","99101"], ["500","53","99102"], ["497","53","99103"], ["61","53","99105"], ["5109","53","99109"], ["1667","53","99110"], ["4047","53","99111"], ["761","53","99113"], ["12018","53","99114"], ["1143","53","99115"], ["1439","53","99116"], ["485","53","99117"], ["931","53","99118"], ["1204","53","99119"], ["145","53","99121"], ["3773","53","99122"], ["1058","53","99123"], ["240","53","99124"], ["527","53","99125"], ["877","53","99126"], ["278","53","99128"], ["793","53","99129"], ["828","53","99130"], ["170","53","99131"], ["1493","53","99133"], ["643","53","99134"], ["255","53","99135"], ["29","53","99136"], ["343","53","99137"], ["1209","53","99138"], ["1301","53","99139"], ["475","53","99140"], ["5240","53","99141"], ["650","53","99143"], ["43","53","99144"], ["56","53","99146"], ["190","53","99147"], ["2106","53","99148"], ["208","53","99149"], ["380","53","99150"], ["164","53","99151"], ["197","53","99152"], ["467","53","99153"], ["20","53","99154"], ["1350","53","99155"], ["8114","53","99156"], ["697","53","99157"], ["603","53","99158"], ["1375","53","99159"], ["149","53","99160"], ["1400","53","99161"], ["31404","53","99163"], ["0","53","99164"], ["3222","53","99166"], ["625","53","99167"], ["2459","53","99169"], ["1072","53","99170"], ["959","53","99171"], ["1532","53","99173"], ["44","53","99174"], ["145","53","99176"], ["437","53","99179"], ["939","53","99180"], ["1901","53","99181"], ["1295","53","99185"], ["13342","53","99201"], ["21580","53","99202"], ["20192","53","99203"], ["6681","53","99204"], ["42137","53","99205"], ["34802","53","99206"], ["30854","53","99207"], ["49193","53","99208"], ["19197","53","99212"], ["24362","53","99216"], ["17423","53","99217"], ["15531","53","99218"], ["29273","53","99223"], ["18289","53","99224"], ["68191","53","99301"], ["9201","53","99320"], ["531","53","99321"], ["217","53","99322"], ["3684","53","99323"], ["9202","53","99324"], ["4956","53","99326"], ["3733","53","99328"], ["160","53","99329"], ["907","53","99330"], ["50","53","99333"], ["340","53","99335"], ["48753","53","99336"], ["29845","53","99337"], ["11555","53","99338"], ["884","53","99341"], ["3040","53","99343"], ["16817","53","99344"], ["213","53","99345"], ["352","53","99346"], ["2293","53","99347"], ["1364","53","99348"], ["8483","53","99349"], ["12979","53","99350"], ["26975","53","99352"], ["13306","53","99353"], ["21343","53","99354"], ["294","53","99356"], ["4323","53","99357"], ["158","53","99359"], ["1350","53","99360"], ["1902","53","99361"], ["41010","53","99362"], ["216","53","99363"], ["311","53","99371"], ["273","53","99401"], ["1875","53","99402"], ["19548","53","99403"]],
      oneZipData : [["P0010001","state","zip code tabulation area"],["24348","53","98029"]]
    };

  }));


  beforeEach(inject(function(_$http_, _$httpBackend_) {
    $http = _$http_;
    $httpBackend = _$httpBackend_;
    $httpBackend.whenGET(urls[0]).respond(responsesData.allZips);
    $httpBackend.flush();
  }));

  afterEach(function() {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });


  function getData(inputParameters) {

    $httpBackend.whenGET(urls[inputParameters.urlIndex]).respond(responsesData[inputParameters.responseData]);

    scope.userSelection = inputParameters.zipNumber;
    scope.userSelection2 = inputParameters.category;

    scope[methodsName[inputParameters.methodIndex]]();

    $httpBackend.flush();
  }


  it('proves that we can run test for ZipsCtrl', function () {
    expect(true).toBe(true);
  });


  it('proves that ZipsCtrl has function called "getZipPopulation" ', function () {
    expect(typeof scope.getZipPopulation).toBe("function");
  });

  it('should get Issaquah population', function () {

    var inputParameters = {
      urlIndex : 1,
      methodIndex : 0,
      zipNumber : ["24348","53","98029"],
      category : null,
      responseData : "oneZipData"
    };


    getData(inputParameters);

    expect(scope.pop).toBe("24348");
  });



});

